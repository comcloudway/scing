import React from 'react';

import WelcomeActivity from './routes/WelcomeActivity';
import MainActivity from './routes/MainActivity';
import AppLoadingActivity from './routes/AppLoadingActivity';
import {LanguageContext, LANGUAGES} from './lib/translate';

function App() {
  const [view, setView] = React.useState(<AppLoadingActivity/>);

  const [lang, setLang] = React.useState(localStorage.getItem("LANG")||"en");

  React.useEffect(()=>{
    let url = localStorage.getItem("SUPABASE_URL") || false;
    let key = localStorage.getItem("SUPABASE_KEY") || false;
    let uname = localStorage.getItem("USER") || false;
    if (!url || !key || !uname) {
      // yet to log in
      setView(<WelcomeActivity updateRootView={setView}/>);
    } else {
      // logged in
      setView(<MainActivity/>);
    };

    let i = setInterval(()=>{
      let n = localStorage.getItem("LANG")||"en";
      if(n!==lang.ID && LANGUAGES[n]) setLang(n);
    }, 1000);
    return ()=>{
      clearInterval(i)
    }
  }, []);


  return (
    <div style={{
      width: '100%',
      height: '100%',
      position: 'absolute',
      top: 0,
      left: 0,
    }}>
    <LanguageContext.Provider value={LANGUAGES[lang]}>
      {view}
    </LanguageContext.Provider>
    </div>
  );
}
window.addEventListener('beforeinstallprompt', (e) => {
  // Prevent Chrome 67 and earlier from automatically showing the prompt
  e.preventDefault();
  // Stash the event so it can be triggered later.
  window.deferredPrompt = e;
});
export default App
