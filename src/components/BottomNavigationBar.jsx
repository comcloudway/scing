import * as React from 'react';

export const createBottomNavigationBarItem = (key, icon, text) => {
    return {
        key:key,
        icon:icon,
        text:text
    }
}

export function BottomNavigationBarItem({focused, icon, text, style={}, colors:[def="black", foc="lightblue"], ...args}) {
    const [hovered, setHovered] = React.useState(false);
    return (
        <div style={{
            opacity: hovered?0.8:1,
            transition: 'opacity .2s, color .4s',
            display:'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'space-around',
            color: focused?foc:def,
            ...style
        }} {...args} onMouseEnter={()=>setHovered(true)} onMouseLeave={()=>setHovered(false)}>
          {React.createElement(icon, {size: 28})}
          <p style={{
              transform: `scaleY(${focused?1:0})`,
              transition: 'transform .4s linear 10s',
              padding: 0,
              margin: '0.1rem',
              display: focused?'':'none'
          }}>{text}</p>
        </div>
    );
}

export default function BottomNavigationBar({background, style={}, theme:child_colors=[], children=[], index, onClick=()=>{}, child_style={}, ...args}) {
    return (
        <div style={{
            background: background||"whitesmoke",
            width: 'calc(100% - 2rem)',
            height: '3rem',
            position: 'fixed',
            padding: '0.4rem 1rem',
            bottom: 0,
            left: 0,
            display: 'flex',
            alignItems: 'center',
            justifyContent: 'space-evenly',
            ...style
        }} {...args}>
          {children.map(({key, icon, text}, i)=>{
              return <BottomNavigationBarItem focused={i===index} onClick={()=>onClick(key)} colors={child_colors} icon={icon} text={text} style={child_style}/>
          })}
        </div>
    );
}
