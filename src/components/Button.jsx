import * as React from 'react';

export default function Button({background, hover, color, text, style, ...args}) {
    const [hovered, setHovered] = React.useState(false);
    return (
        <div onMouseEnter={()=>setHovered(true)} onMouseLeave={()=>setHovered(false)} style={{
            background: hovered?hover||'light-grey':(background || 'grey'),
            border: `2px solid ${hover}`,
            padding: '0.6rem 0.8rem',
            margin: '0.4rem',
            borderRadius: '6px',
            color: color|| "black",
            transition: 'background .4s',
            cursor: 'pointer',
            ...style
        }} {...args}>{text}</div>
    );
}
