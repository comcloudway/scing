import * as React from 'react';

export default function Card({background, hover, shadow, children, style, ...args}) {
    const [hovered, setHovered] = React.useState(false);
    return (
        <div onMouseEnter={()=>setHovered(true)} onMouseLeave={()=>setHovered(false)} style={{
            background: hovered?hover||'light-grey':(background || 'grey'),
            boxShadow: `0px 0px 7px -5px ${shadow || 'rgba(0,0,0,0.3)'}`,
            padding: '0.8rem',
            margin: '0.4rem',
            borderRadius: '8px',
            transition: 'background .4s',
            ...style
        }} {...args}>{children}</div>
    );
}
