import React from 'react';
import Webcam from "react-webcam";
import * as Feather from 'react-feather';

import * as GRUVBOX from '../lib/gruvbox-colors';
import IconButton from '../components/IconButton';
import Card from '../components/Card';

export default function EditPopup({edit, name, content={}, onCancel=()=>{}, onSave=()=>{}, set=()=>{}, state={groups:[]}, enogroup}) {

      const groups_object = React.useMemo(()=>{

        let o = {};

        state.groups.forEach(obj=>{
            o[obj.id]={...obj};
            delete o[obj.id].id
        });

        return o;
    }, [state.groups])


  const [loading, setLoading] = React.useState(false);

    const webcamRef = React.useRef(null);
    const cfm = React.useRef("user");

    const [camera_facing_mode, setCFM] = React.useState("user");

    const updateContent = (cb=(s)=>s) => {
        set(x=>{
            x=cb(x)
            return x;
        })
    }

    const capture = React.useCallback(
        ()=>{
            const imageSrc = webcamRef.current.getScreenshot();
            updateContent(x=>{
                if(!x.thumbnail) return;
                x.thumbnail.data=imageSrc;
                return x;
            })
        },
        [webcamRef, updateContent]
    )

    return (
        <div style={{
            overflowY:'auto'
        }}>
          <div style={{
              position: 'fixed',
              bottom: !!edit?0:'-200vh',
              left: 0,
              background: GRUVBOX.dark1,
              height: '100%',
              width:'100vw',
              zIndex:10,
              transition:'bottom .2s',
              overflowY:'auto'
          }}>
            {/*Popup Top Bar*/}
            <div style={{
                width: 'calc(100% - 2rem)',
                padding: '0 1rem',
                position:'sticky',
                marginBottom:'1rem',
                top:0,
                left:0,
                height:'4rem',
                display:'flex',
                alignItems:'center',
                justifyContent:'space-between',
                background: GRUVBOX.dark0_hard,
                zIndex:10
            }}>
              <p style={{
                  fontWeight:'bold'
              }}>{name||edit}</p>
              <div style={{
                  display:'flex',
                  alignItems:'center'
              }}>
                <IconButton onClick={()=>{
                    if(window.confirm("Do you really want to discard your changes?")) {
                        //cancel
                        onCancel();
                    }
                }} icon={<Feather.X/>} color={GRUVBOX.light0} hover={GRUVBOX.dark2} dimens={['2rem', '2rem']} style={{padding:'0.4rem'}}/>
                <IconButton onClick={()=>{
                    if(window.confirm("Do you really want to save your changes?")) {
                        // Save
                        onSave();
                    }
                }} icon={<Feather.Check/>} color={GRUVBOX.light0} hover={GRUVBOX.dark2} dimens={['2rem', '2rem']} style={{padding: '0.4rem'}}/>
              </div>
            </div>

            {/*ScrollContainer*/}
            <div
              style={StyleSheet.container}>

              {/*Webcam / Thumbnail View*/}
              {!!content.thumbnail&&<Card style={StyleSheet.imager} children={[
                  ...[!!content.thumbnail.data?(<img src={content.thumbnail.data} alt={name||edit} style={StyleSheet.thumbnail}/>):''],
                  ...[(!content.thumbnail.data&&edit)?
                      (<Webcam
                                         audio={false}
                                         height={720}
                                         ref={webcamRef}
                                         screenshotFormat="image/png"
                                         width={1280}
                                         videoConstraints={{
                                             facingMode: camera_facing_mode
                                         }}
                                         style={StyleSheet.thumbnail}
      />):''],
                  <div style={{
                      display:'flex',
                      alignItems:'center',
                      justifyContent:'space-evenly'
                  }}>
                    {!content.thumbnail.data&&<IconButton dimens={['2rem', '2rem']} icon={<Feather.Camera/>} color={GRUVBOX.light0} hover={GRUVBOX.dark2} onClick={()=>{
                        capture()
                    }}/>}
                    {!content.thumbnail.data&&<IconButton dimens={['2rem', '2rem']} icon={<Feather.Repeat/>} color={GRUVBOX.light0} hover={GRUVBOX.dark2} onClick={()=>{
                        if(cfm.current==="user") {
                            setCFM({exact: "environment"})
                            cfm.current="env"
                        } else {
                            setCFM("user");
                            cfm.current="user"
                        }
                    }}/>}
                    {!!content.thumbnail.data&&<IconButton dimens={["2rem", "2rem"]} icon={<Feather.CameraOff/>} onClick={()=>{
                        updateContent(x=>{
                            x.thumbnail.data=null;
                            return x;
                        })
                    }} hover={GRUVBOX.dark2} color={GRUVBOX.light0}/>}
                  </div>
              ]} background={GRUVBOX.dark0}/>}
              <div style={StyleSheet.inputs}>
                {Object.entries(content).filter(inp=>!(['id', 'thumbnail'].includes(inp[0]))).map(([key, value])=>{
                    let field = '';
                    switch(value.type) {
                        case "textarea":
                            field = (<textarea key={key+'textarea'} style={{
                                outline:'none',
                                border: 'none',
                                borderLeft: '1rem solid ' + GRUVBOX.neutral_aqua,
                                background: GRUVBOX.dark0,
                                color: GRUVBOX.light1,
                                padding:'0.8rem 0.4rem',
                                flex: 1,
                                margin: '1rem',
                                borderRadius: 8
                            }} onChange={e=>{
                                updateContent(c=>{
                                    c[key].data=e.target.value;
                                    return c;
                                });
                            }} defaultValue={content[key].data}></textarea>)
                            break
                        case "input":
                        default:
                        field = (<input id={key} key={key+'input'} type={(typeof content[key].data === "number")?'nunber':'text'} style={{
                                outline:'none',
                                border: 'none',
                                borderLeft: '2rem solid ' + GRUVBOX.neutral_aqua,
                                background: GRUVBOX.dark0,
                                color: GRUVBOX.light1,
                                padding:'0.8rem 0.4rem',
                                flex: 1,
                                margin: '1rem',
                                borderRadius: 8
                            }} placeholder={value.placeholder} defaultValue={content[key].data} onChange={e=>{
                                e.preventDefault();
                                updateContent(c=>{
                                    c[key].data=e.target.value;
                                    return c;
                                });
                            }}/>)
                    }
                  const isGroup = (id,g) => {
                    g.forEach(g=>{
                      if (!id) return false;
                      console.log(id.value==g.id)
                      if(g.id==id.value) return true
                    })
                    return false
                  }
                    return (
                        <div key={key} style={{
                          display:'flex',
                          alignItems:'center'
                        }}>
                          <p style={{
                              margin: 0,
                              paddingTop: '0.4rem'
                          }}>{(value.placeholder||key).toUpperCase()}:</p>
                          {field}
                          {key==="group"&&!loading&&document.getElementById(key)&&!groups_object[document.getElementById(key).value]&&<div><IconButton onClick={async ()=>{
                            if(state.supabase) {
                              setLoading(true);
                              const { error } = await state.supabase.client
                                    .from('Groups')
                                    .insert([
                                      { id:content[key].data.trim(), color:"",min:0,tags:[],priority:1 },
                                    ]);
                              setLoading(false);
                              if(error) {
                                alert(enogroup||"Failed to create Group")
                              }
                            }

                          }} dimens={['2rem', '2rem']} color={GRUVBOX.light4} hover={GRUVBOX.dark4} icon={<Feather.Plus/>} style={{margin:0}}/></div>}
                          {key==="group"&&!loading&&document.getElementById(key)&&document.getElementById(key).value!==content.id.data&&!groups_object[document.getElementById(key).value]&&<div><IconButton onClick={async ()=>{
                                                      document.getElementById(key).value=content.id.data
                          }} dimens={['2rem', '2rem']} color={GRUVBOX.light4} hover={GRUVBOX.dark4} icon={<Feather.Link/>} style={{margin:0}}/></div>}
                        </div>
                    );
                })}
              </div>
            </div>
          </div>
        </div>
    );
}
const StyleSheet = {
    thumbnail: {
        minWidth:'55vmin',
        maxWidth:'75vmin',
        position:'relative',
        height:'auto',
        borderRadius:12.5
    },
    container: {
        display:'flex',
        flexWrap:'wrap',
        alignItems:'center',
        justifyContent:'space-evenly',
        height:'calc(100% - 4rem)'
    },
    imager: {
        display: 'flex',
        flexDirection:'column',
    },
    inputs: {
        display:'flex',
        flexDirection:'column',
        alignItems:'flex-start',
        justifyContent:'space-evenly'
    }
}
