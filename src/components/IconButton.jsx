import * as React from 'react';

export default function IconButton({background="transparent", hover, icon, color, dimens: [height, width], style, ...args}) {
    const [hovered, setHovered] = React.useState(false);
    return (
        <div onMouseEnter={()=>setHovered(true)} onMouseLeave={()=>setHovered(false)} style={{
            height: height || '2rem',
            width: width || '2rem',
            margin: '0.4rem',
            padding: '0.8rem',
            borderRadius: '50%',
            background: hovered?hover||"grey":background,
            transition:'background .4s',
            display:'flex',
            alignItems:'center',
            justifyContent:'center',
            color: color || "white",
            ...style
        }} {...args}>{icon}</div>
    );
}
