import * as React from 'react';
import {ShoppingCart as ShoppingCartIcon} from 'react-feather';

export default function Loader({show, style, background, max, color, start=50, ...args}) {
    const [pos, setPos] = React.useState(start);
    const hold = React.useRef(pos);
    const mod = React.useRef(1);

    React.useEffect(()=>{
        let i = setInterval(()=>{
            let step = 5;
            hold.current +=  mod.current*step;
            if (hold.current > 100 + step) {
                mod.current = -1;
            } else if (hold.current < 0 - step){
                mod.current = 1;
            }

            setPos(hold.current);
        }, 200);
        return ()=>{
            clearInterval(i);
        }
    }, [])

    return (
        <div style={{
            background: background || "dark-grey",
            width: '100%',
            height: '100%',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'center',
            position: 'fixed',
            bottom:0,
            left: 0,
            transition: 'opacity .4s',
            borderTopLeftRadius: 8,
            borderTopRightRadius: 8,
            opacity: show?max||0.8:0,
            color: color||"black",
            ...style
        }} {...args}>
          <ShoppingCartIcon size={92} style={{
              position: 'absolute',
              top: '50%',
              left: pos+'%',
              transition: 'left 1s',
              transform: `translate(-${pos}%, -50%) scaleX(${mod.current})`
          }}/>
        </div>
    );
}
