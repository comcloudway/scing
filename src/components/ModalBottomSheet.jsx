import * as React from 'react';

import Button from './Button';

export default function ModalBottomSheet({background, shadow, children=[], style={}, show:open=true, buttons:[[button1_text, button1_onclick, button1_bg="yellow", button1_clr="white"],[button2_text, button2_onclick, button2_bg="blue", button2_clr="white"]], ...args}) {
    return (
        <div style={{
            background: background || "dark-grey",
            width: 'calc(100% - 5rem)',
            padding: '1rem',
            minHeight: '20vh',
            maxHeight: '600px',
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
            justifyContent: 'space-evenly',
            position: 'fixed',
            bottom: open?0:'-200vh',
            left: '50%',
            transform: 'translateX(-50%)',
            transition: 'bottom .4s, box-shadow .2s',
            borderTopLeftRadius: 8,
            borderTopRightRadius: 8,
            boxShadow: open?`0px 0px 100vh 50vh ${shadow}`:'',
            ...style
        }}>
          <div>{children}</div>
          <div style={{
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'space-evenly'
          }}>
            <Button style={StyleSheet.buttons} text={button1_text} onClick={()=>button1_onclick()} background="transparent" hover={button1_bg} color={button1_clr}/>
            <Button style={StyleSheet.buttons} text={button2_text} onClick={()=>button2_onclick()} background="transparent" hover={button2_bg} color={button1_clr}/>
          </div>
        </div>
    );
}
const StyleSheet = {
    buttons: {
        display:'flex',
        alignItems: 'center',
        justifyContent: 'center'
    }
}
