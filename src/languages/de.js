//
// LANGUAGE: German
// The SCING German Language
// File translated and created by @comcloudway
// File cloned from the English version
//

export const LANGUAGE = "Deutsch";
export const ID = "de";

// Translations for Welcome Screen
export const WelcomeActivity = {
    scan: "Scannen",
    connect: "Verbinden"
}

// Translations from Join Screen
export const JoinActivity = {
    back: "Zurück",
    failed: {
        message: "Wir konnten leider keinen validen Einladungscode erkennen",
        cancel: "Abbrechen",
        tryharder: "Erneut versuchen",
        invalid: "Supabase anmeldedaten falsch"
    },
    success: {
        message: "Du wurdest von {} eingeladen, seinem SCING beizutreten",
        cancel: "Abbrechen",
        accept: "Akzeptieren"
    }
}

// Translations for Connnect Screen
export const ConnectActivity = {
    nickname: "Gib einen Nutzernamen ein",
    serverurl: "Gib die Server URL an",
    serverkey: "Gib den Server Schlüssel an",
    title: "Supabase Konfigurator",
    back: "Zurück",
    connect: "Verbinden",
    errors: {
        auth: "Authentifikation fehlgeschlagen",
        data: "Konnte keine Supabaseinstanz finden",
        user: "Dein Nutzername ist zu kurz"
    }
}

// Translations for MainActivity Screen
export const MainActivity = {
    categories: {
        scan: "Scannen",
        groups: "Gruppen",
        products: "Produkte",
        recipes: "Rezepte",
        tools: "Weitere"
    },
    bored: "Dauert es dir zu lange? Möchtest du dich abmelden?"
}

// Translations for Scan Tab
export const ProductScanView = {
    tryharder: "Erneut versuchen",
    code: "Produktnummer",
    createproduct: "Produkt erstellen",
    addtostock: "Ins Regal stellen",
    delfromstock: "Aus dem Regal nehmen",
    errors: {
        createproductexists: "Du hast das Produkt bereits erstellt",
        createproducterror: "Ein Fehler beim erstellen des Produktes trat auf",
        addtostockerror: "Konnte das Produkt nicht ins Regal räumen",
        delfromstockerror: "Konnte das Produkt nicht aus dem Regal nehmen - vielleicht ist es umgefallen..",
        delfromstacknothingleft: "Dieses Produkt steht nicht in deinem Regal"
    },
    success: {
        addedtostock: "Erfolgreich im Regal eingeräumt",
        delfromstock: "Erfolgreich aus dem Regal genommen"
    },
    addtostockamount: "Anzahl",
    camera: {
        next: "Nächste",
        prev: "Vorherige"
    }
}

// Translations for Groups Tab
export const GroupsView = {
    saf: { // translations for search&filter block
        filter: "Filter",
        search: "Suche",
        sortmodes: ['Alphabet A->Z', 'Alphabet Z->A', 'Priorität++','Priorität--', 'Verfügbarkeit++', 'Verfügbarkeit--'],
        f: {
            prev: "Vorheriger",
            next: "Nächster",
            pre: "Sortiert nach: "
        },
        clear: "Leeren"
    },
    nogroups: "Keien Gruppen gefunden",
    unnamed: "Unbenannte Gruppe",
    edit: { // translation for edit popup
        error:"Ein Fehler trat auf",
        errors: {
            save: "Konnte die Gruppe nicht speichern"
        }
    },
    opts: { // Options performed on per item basis
        view: "Ansehen",
        edit: "Bearbeiten",
        delete: "Löschen"
    },
    delete: {
        confirm: "Möchtest du {} wirklich löschen?",
        error: "Konnte die Gruppe nicht entfernen"
    },
    add: {
        prompt: "Gib eine GruppenID an",
    },
    reader: {
        close: "Schließen",
        opts: {
            remove: "Aus Gruppe entfernen"
        },
        unnamed: "Unbenanntes Produkt",
        remove: {
            error: "Konnte das Produkt nicht aus der Gruppe entfernen",
            confirm: "Möchtest du {} wirklich aus der Gruppe entfernen"
        }
    }
}

// Transaltions for Products Tab
export const ProductsView = {
    saf: { // translations for search&filter block
        filter: "Filter",
        search: "Suche",
        sortmodes: ['Alphabet A->Z', 'Alphabet Z->A', 'Verfügbarkeit++', 'Verfügbarkeit--'],
        f: {
            prev: "Vorheriger",
            next: "Nächster",
            pre: "Sortiert nach: "
        },
        clear: "Leeren"
    },
    edit: {
        error: "Konnte das Produkt nicht speichern",
        enogroup: "Konnte die Gruppe nicht erstellen"
    },
    unnamed: 'Unbenanntes Produkt',
    noproducts: 'Keine Produkte gefunden',
    opts: { // Options performed on per item basis
        edit: "Bearbeiten",
        delete: "Löschen",
            group: "Gruppieren",
        ungroup: "Gruppe lösen"
    },
    delete: {
        confirm: "Möchtest du {} wirklich löschen",
        error: "Konnte das Produkt nicht löschen"
    },
    group:{
        confirm: "Möchtest du {} wirklich gruppieren?",
        error: "Konnte die Gruppe nicht erzeugen"
    },
    ungroup: {
        confirm: "Möchtest du die Gruppe von {} wirklich lösen?",
        error: "Konnte die Gruppe nicht lösen"
    }
}

// Translations for Recipe Tab
export const RecipeView = {
 saf: { // translations for search&filter block
        filter: "Filter",
        search: "Suche",
        sortmodes: ['Alphabet A->Z', 'Alphabet Z->A', 'Verfügbarkeit++', 'Verfügbarkeit--'],
        f: {
            prev: "Vorheriger",
            next: "Nächster",
            pre: "Sortiert nach: "
        },
        clear: "Leeren"
    },
    norecpies: "Keine Rezepte gefunden",
    unnamed: "Unbenanntes Rezept",
    reader: {
        unnamedprod: "Unbenanntes Produkt",
        exit: "Schließen",
        prev: "Vorherige",
        next: "Nächste"
    },
    edit: { // translation for edit popup
        errors: {
            save: "Konnte das Rezept nicht speichern"
        }
    },
    opts: { // Options performed on per item basis
        view: "Lesen",
        edit: "Bearbeiten",
        delete: "Löschen"
    },
    status: {
        missing: "Fehlende Zutaten",
        hasitall: "Alle Zutaten vorhanden"
    },
    delete: {
        confirm: "Möchtest du {} wirklich löschen?",
        error: "Konnte das Rezept nicht löschen"
    },
}

// Translations for Tools Tabs
export const ToolsView = {
    share: {
        title: "Tritt meinem SCING bei",
        text: "Scan diesen Code mit einem SCING Client deiner Wahl. Die offizielle WebApp findest du hier: https://scing.ccw.icu",
        cta: "Lasse einen Freund diesen Code scannen oder teile den Code mit ihm, damit er deinem SCING beitreten kann",
        unsupported: "Das Teilen dieses Codes ist auf deinem Gerät leider nicht unterstützt"
    },
    logout: "Abmelden",
    install: "Installieren",
    lang: {
        text: "Sprache: ",
        reset: "Zurücksetzten",
        save: "Speichern"
    }
}
