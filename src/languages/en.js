//
// LANGUAGE: English
// The SCING English Language
// File translated and created by @comcloudway
//

export const LANGUAGE = "English";
export const ID = "en";

// Translations for Welcome Screen
export const WelcomeActivity = {
    scan: "Scan",
    connect: "Connect"
}

// Translations from Join Screen
export const JoinActivity = {
    back: "Back",
    failed: {
        message: "Unfortunately we were unable to detect a valid invite code",
        cancel: "Cancel",
        tryharder: "Try harder",
        invalid: "Invalid Supabase Data"
    },
    success: {
        message: "You were invited by {} to join his SCING",
        cancel: "Cancel",
        accept: "Accept"
    }
}

// Translations for Connnect Screen
export const ConnectActivity = {
    nickname: "Enter a username",
    serverurl: "Enter the server url",
    serverkey: "Enter the server key",
    title: "Supabase Configurator",
    back: "Back",
    connect: "Connect",
    errors: {
        auth: "Authentification failed",
        data: "Couldn't contact Supabase Instance",
        user: "Your username is to short <2"
    }
}

// Translations for MainActivity Screen
export const MainActivity = {
    categories: {
        scan: "Scan",
        groups: "Groups",
        products: "Products",
        recipes: "Recipes",
        tools: "Tools"
    },
    bored: "Do you feel your request is taking to long? - Want to log out?"
}

// Translations for Scan Tab
export const ProductScanView = {
    tryharder: "Try harder",
    code: "Productcode",
    createproduct: "Add Product",
    addtostock: "Add to Stock",
    delfromstock: "Remove from Stock",
    errors: {
        createproductexists: "You have already created the product",
        createproducterror: "Couldn't create Product",
        addtostockerror: "Couldn't throw the product onto your stock-pile",
        delfromstockerror: "Failed to remove the item from the stock pile - hopefully it didn't collapse",
        delfromstacknothingleft: "You do not have this item in stock"
    },
    success: {
        addedtostock: "Successfully sorted product into your drawer",
        delfromstock: "Successfully removed item"
    },
    addtostockamount: "Amount",
    camera: {
        next: "Next",
        prev: "VPrevious"
    }
}

// Translations for Groups Tab
export const GroupsView = {
    saf: { // translations for search&filter block
        filter: "Filter",
        search: "Search",
        sortmodes: ['Alphabet A->Z', 'Alphabet Z->A','Priority++','Priority--', 'Availability++', 'Availability--'],
        f: {
            prev: "Previous",
            next: "Next",
            pre: "Sorting by: "
        },
        clear: "Reset"
    },
    nogroups: "No groups found",
    unnamed: "Unnamed Group/Products",
    edit: { // translation for edit popup
        error:"Ein Fehler trat auf",
        errors: {
            save: "Unable to save the group"
        }
    },
    opts: { // Options performed on per item basis
        view: "View",
        edit: "Edit",
        delete: "Delete"
    },
    delete: {
        confirm: "Do you really want to delte {}?",
        error: "Couldn't delte item"
    },
    add: {
        prompt: "Enter a groupID",
    },
    reader: {
        close: "Close",
        opts: {
            remove: "Remove from Group"
        },
        unnamed: "Unnamed Product",
        remove: {
            error: "Couldn't remove Product from group",
            confirm: "Do you really want to remove {} from the group?"
        }
    }
}

// Transaltions for Products Tab
export const ProductsView = {
    saf: { // translations for search&filter block
        filter: "Filter",
        search: "Search",
        sortmodes: ['Alphabet A->Z', 'Alphabet Z->A', 'Availability++', 'Availability--'],
        f: {
            prev: "Previous",
            next: "Next",
            pre: "Sorting by: "
        },
        clear: "Reset"
    },
    edit: {
        error: "Unable to save the item",
        enogroup: "Couldn't create group"
    },
    unnamed: 'Unnamed Product',
    noproducts: 'No products fround',
    opts: { // Options performed on per item basis
        edit: "Edit",
        delete: "Delete",
        group: "Group",
        ungroup: "Ungroup"
    },
    delete: {
        confirm: "Do you really want to delte {}?",
        error: "Couldn't delte item"
    },
    group:{
        confirm: "Do you really want to group {}?",
        error: "Couldn't group item"
    },
    ungroup: {
        confirm: "Do you really want to ungroup {}?",
        error: "Failed to ungroup item"
    }
}

// Translations for Recipe Tab
export const RecipeView = {
 saf: { // translations for search&filter block
        filter: "Filter",
        search: "Search",
        sortmodes: ['Alphabet A->Z', 'Alphabet Z->A', 'Availability++', 'Availability--'],
        f: {
            prev: "Previous",
            next: "Next",
            pre: "Sorting by: "
        },
        clear: "Reset"
    },
    norecpies: "No recipes found",
    unnamed: "Unnamed Recipe",
    reader: {
        unnamedprod: "Unnamed Product",
        exit: "Exit",
        prev: "Previous",
        next: "Next"
    },
    edit: { // translation for edit popup
        errors: {
            save: "Unable to save the recipe"
        }
    },
    opts: { // Options performed on per item basis
        view: "Read",
        edit: "Edit",
        delete: "Delete"
    },
    status: {
        missing: "Missing ingredients",
        hasitall: "You have all the required stuff"
    },
    delete: {
        confirm: "Do you really want to delte {}?",
        error: "Couldn't delte item"
    },
    add: {
//        prompt: "Enter a groupID",
    }
}

// Translations for Tools Tabs
export const ToolsView = {
    share: {
        title: "Join my SCING",
        text: "Scan this code with your SCING client of choice to join my SCING. You can use the WebApp over on https://scing.ccw.icu",
        cta: "Have a friend scan this code, or try sharing it with him, so he is able to join your SCING",
        unsupported: "Unfortunately sharing isn't supported on your platform"
    },
    logout: "Logout",
    install: "Install",
    lang: {
        text: "Language: ",
        reset: "Reset",
        save: "Save"
    }
}
