export const dark0_hard  = '#1d2021';
export const dark0       = '#282828';
export const dark0_soft  = '#32302f';
export const dark1       = '#3c3836';
export const dark2       = '#504945';
export const dark3       = '#665c54';
export const dark4       = '#7c6f64';
export const dark4_256   = '#7c6f64';

export const gray_245    = '#928374';
export const gray_244    = '#928374';

export const light0_hard = '#f9f5d7';
export const light0      = '#fbf1c7';
export const light0_soft = '#f2e5bc';
export const light1      = '#ebdbb2';
export const light2      = '#d5c4a1';
export const light3      = '#bdae93';
export const light4      = '#a89984';
export const light4_256  = '#a89984';

export const bright_red     = '#fb4934';
export const bright_green   = '#b8bb26';
export const bright_yellow  = '#fabd2f';
export const bright_blue    = '#83a598';
export const bright_purple  = '#d3869b';
export const bright_aqua    = '#8ec07c';
export const bright_orange  = '#fe8019';

export const neutral_red    = '#cc241d';
export const neutral_green  = '#98971a';
export const neutral_yellow = '#d79921';
export const neutral_blue   = '#458588';
export const neutral_purple = '#b16286';
export const neutral_aqua   = '#689d6a';
export const neutral_orange = '#d65d0e';

export const faded_red      = '#9d0006';
export const faded_green    = '#79740e';
export const faded_yellow   = '#b57614';
export const faded_blue     = '#076678';
export const faded_purple   = '#8f3f71';
export const faded_aqua     = '#427b58';
export const faded_orange   = '#af3a03';
