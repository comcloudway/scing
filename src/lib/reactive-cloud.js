import React from 'react';

export const useBigState = initial => {
    const [state, dispatch] = React.useReducer((state, action) => { return action.payload }, initial);
    const setState = (obj = {}) => {
        dispatch({
            type: "",
            payload: obj
        })
    };
    const socket = React.useRef([]);
    const set = cb => {
        let id = socket.current.length;
        socket.current[id] = new Promise(async res => {
            let x = state
            if (id !== 0) x = await socket.current[id - 1];
            x = { ...x }
            let r = cb(x);
            if (typeof r !== "object") return;
            setState(r);
            res(r)
        })
    };

    return [state, set];
}
