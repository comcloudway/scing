export const format = (...args) => {
    if(args.length===0) {
        throw new Error("No Arguments passed to format");
    }
    if(typeof args[0]==="string") {
        let r = args[0];
        args.slice(1).forEach(d=>r=r.replace("{}",d));
        return r;
    } else {
        throw new Error(`Format cannot be used on type: ${typeof args[0]}`)
    }
}
