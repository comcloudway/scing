import React from 'react';

import * as LANG_EN from '../languages/en';
import * as LANG_DE from '../languages/de';

export const LANGUAGES = {
    en: LANG_EN,
    de: LANG_DE
}
export const LanguageContext = React.createContext(LANGUAGES.en);
