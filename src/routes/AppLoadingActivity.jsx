/*
 * View shown whilst loading App
 * */
import * as React from 'react';

import * as GRUVBOX from '../lib/gruvbox-colors';

import logo from '../assets/logo.png';

export default function AppLoadingActivity() {
  return (
    <div style={{
      height: '100%',
      width: '100%',
      display: 'flex',
      alignItems:'center',
      justifyContent: 'center',
      background: GRUVBOX.dark0_hard,
      top: 0,
      left: 0,
      position: 'fixed'
    }}>
      <img src={logo} style={{
        width: '12.5rem',
        height: '12.5rem'
      }} alt="Logo"/>
    </div>
  );
}
