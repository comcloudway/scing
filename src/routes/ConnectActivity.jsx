/*
 * View shown if user hasn't been invited, but wants to connect to his own supabase DB
 * */
import * as React from 'react';
import * as Feather from 'react-feather';
import { createClient } from '@supabase/supabase-js'

import Loader from '../components/Loader';
import * as GRUVBOX from '../lib/gruvbox-colors';
import Button from '../components/Button';
import {LanguageContext} from '../lib/translate';

export default function ConnectActivity({SUPABASE_KEY="", SUPABASE_URL="", USER="", back_action=()=>window.location.reload()}) {

    const [user, setUser] = React.useState(USER);
    const [supabaseUrl, setSupabaseUrl] = React.useState(SUPABASE_URL);
    const [supabaseKey, setSupabaseKey] = React.useState(SUPABASE_KEY);
    const [loading, setLoading] = React.useState(false);
    const lang = React.useContext(LanguageContext);

    return (
        <div style={{
            background: GRUVBOX.dark0_hard,
            height: '100%',
            width: '100%',
            display: 'flex',
            flexDirection: 'column',
            flexWrap: 'wrap',
            alignItems: 'center',
            justifyContent: 'space-evenly',
            color: GRUVBOX.light0,
            overflowY:'auto'
        }}>
          <div style={{
              display: 'flex',
              alignItems: 'center',
              marginBottom: '1rem'
          }}><Feather.Database size={92}/>
            <h2 style={{
                margin: '0 2rem',
            }}>{lang.ConnectActivity.title||"Setup"}</h2></div>

          <input style={StyleSheet.input} value={user} type="text" placeholder={lang.ConnectActivity.nickname||"Username"} onChange={e=>setUser(e.target.value)}/>
          <input style={StyleSheet.input} disabled={!!SUPABASE_URL?true:false} value={supabaseUrl} type="url" placeholder={lang.ConnectActivity.serverurl||"Server URL"} onChange={e=>setSupabaseUrl(e.target.value.trim().replaceAll(new RegExp("\\s", "g"), ""))}/>
          <input style={StyleSheet.input} disabled={!!SUPABASE_KEY?true:false} value={supabaseKey} type="password" placeholder={lang.ConnectActivity.serverkey||"Server Key"} onChange={e=>setSupabaseKey(e.target.value.trim().replaceAll(new RegExp("\\s", "g"), ""))}/>

          <div style={StyleSheet.buttonbar}>
            <Button style={StyleSheet.buttons} text={lang.ConnectActivity.back||"Back"} onClick={()=>back_action()} background="transparent" hover={GRUVBOX.faded_orange} color={GRUVBOX.light2}/>
            <Button style={StyleSheet.buttons} text={lang.ConnectActivity.connect||"Connect"} onClick={()=>{
                if (user.length>=3) {
                    setLoading(true);
                    const supabase = createClient(supabaseUrl, supabaseKey);
                    try {
                        supabase.from("products")
                                .then(()=>{
                                    // success
                                    localStorage.setItem("SUPABASE_URL", supabaseUrl);
                                    localStorage.setItem("SUPABASE_KEY", supabaseKey);
                                    localStorage.setItem("USER", user);
                                    localStorage.setItem("WAS_INVITED",SUPABASE_KEY!==""&&SUPABASE_URL!=="");
                                    setLoading(false);
                                    window.location.reload();
                                })
                                .catch(e=>{
                                    // error -> auth failed
                                    setLoading(false);
                                    alert(lang.ConnectActivity.errors.auth||"Auth failed")
                                })
                    } catch(e) {
                        setLoading(false);
                        alert(lang.ConnectActivity.errors.data||"Invalid Data")
                    }
                } else {
                    alert(lang.ConnectActivity.errors.user||"Username to short <2")
                }
            }} background="transparent" hover={GRUVBOX.faded_blue} color={GRUVBOX.light2}/>
          </div>

          {loading&&<Loader show={loading} background={GRUVBOX.dark0_hard} color={GRUVBOX.light0}/>}
        </div>
    );
};

const StyleSheet = {
    buttons: {
        display: 'flex',
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    },
    buttonbar: {
        display: 'flex',
        flexDirection: 'row',
        width: '100%',
        alignItems: 'center',
        justifyContent: 'space-evenly',
        flexWrap:'wrap'
    },
    input: {
        background: GRUVBOX.dark1,
        color: GRUVBOX.light3,
        padding: '0.8rem 0.4rem',
        margin: '0.5rem',
        borderRadius: 8,
        border: 'none',
        minWidth: '50vmin',
        borderLeft: `2rem solid ${GRUVBOX.faded_aqua}`,
        outline: 'none'
    }
}
