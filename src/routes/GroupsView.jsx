import * as React from 'react';
import * as Feather from 'react-feather';

import * as GRUVBOX from '../lib/gruvbox-colors';
import IconButton from '../components/IconButton';
import EditPopup from '../components/EditPopup';
import {useBigState} from '../lib/reactive-cloud';
import {LanguageContext} from '../lib/translate';
import {format} from '../lib/rust';

export default function GroupsView({state={}, set=()=>{}, edit_data={id:""}, edit_done=()=>{}}) {
    const [content, setContent] = useBigState(edit_data);
    const [reader, setReader] = useBigState(null);

    const lang = React.useContext(LanguageContext);
    const [filter, setFilter] = React.useState("");
    const [sortMode, setSortMode] = React.useState(0);
    const [barMode, setBarMode] = React.useState(0);

    const product_object = React.useMemo(()=>{

        let o = {};

        state.products.forEach(obj=>{
            o[obj.id]={...obj};
            delete o[obj.id].id
        });

        return o;
    }, [state.products])


    const SORT_FUNCTIONS = [
        // Alphabet
        (arr=[])=>arr.sort((a,b)=>{
            let na = a.name;
            if (products_by_group[a.id]&&products_by_group[a.id][0]) na = products_by_group[a.id][0].name;
            if(!na) na= lang.GroupsView.unnamed || "Unnamed Group";
            let nb = b.name;
            if (products_by_group[b.id]&&products_by_group[b.id][0]) nb = products_by_group[b.id][0].name;
            if(!nb) nb=lang.GroupsView.unnamed || "Unnamed Group";
            na=na.toLowerCase();
            nb=nb.toLowerCase();
            if(na===nb) return 0;
            let arr = [na,nb].sort();
            if(arr[0]===na) return -1;
            return 1
        }),
        (arr=[])=>SORT_FUNCTIONS[0](arr).reverse(),

        // Priority
        (arr=[]) => arr.sort((a,b)=>{
            let pa = a.priority||0;
            let pb = b.priority||0;
            if(pa===pb) return 0;
            if(pa<pb) return -1;
            return 1
        }),
        (arr=[]) => SORT_FUNCTIONS[2](arr).reverse(),

        // Availability
        (arr=[])=>arr.sort((a,b)=>{
            let [ha,hb] = [a,b].map(obj=>{
                let wants = obj.min||0;
                let counter = 0;
                let p = products_by_group[obj.id]||[];
                p.forEach(({stock=0})=>counter+=stock);
                return counter/wants;
            })
            if(ha===hb) return 0;
            if(ha<hb) return -1;
            return 1;
        }),
        (arr=[])=>SORT_FUNCTIONS[4](arr).reverse()
    ];
    const SORT_MODE_NAMES = lang.GroupsView.saf.sortmodes||['Alphabetic A->Z', 'Alphabetic Z->A', 'Priority L->H', 'Priority H_>L', 'Availability L->H', 'Availability H->L'];

    const products_by_group = React.useMemo(()=>{
        let s = {};
        state.products.forEach(obj=>{
            let {group=""} = obj;
            if(!s[group]) s[group]=[];
            s[group].push(obj)
        });
        return s
    }, [state.products])

    const groups = React.useMemo(()=>{
        let s = state.groups.filter(obj=>{
            if(obj.id==="none") return false;
            let name = obj.name
            if (!name && !!products_by_group && !!products_by_group[obj.id] && !!products_by_group[obj.id][0]) {
                name = products_by_group[obj.id][0].name
            }
            if (!name) name=lang.GroupsView.unnamed||"Unnamed Group";
            return (name.toLowerCase().includes(filter.toLowerCase())||
               filter.toLowerCase().includes(name) ||
               (obj.tags && Array.isArray(obj.tags) && obj.tags.filter(t=>t.toLowerCase().includes(filter.toLowerCase())||filter.toLowerCase().includes(t.toLowerCase())).length>0) ||
               (products_by_group[obj.id]&&Array.isArray(products_by_group[obj.id])&&products_by_group[obj.id].filter(({name="", tags=[]})=>(
                   name.toLowerCase().includes(filter.toLowerCase()) ||
                       filter.toLowerCase().includes(name.toLowerCase()) ||
                       tags.map(t=>(t.toLowerCase().includes(filter.toLowerCase())||filter.toLowerCase().includes(t.toLowerCase()))).length>0
               )).length>0)
              );
        });
        let sf = SORT_FUNCTIONS[sortMode];
        if(!sf) sf=SORT_FUNCTIONS[0];
        s = sf(s);
        return s;
    }, [state.groups, products_by_group, filter, sortMode, lang]);

    return (
        <div>
          {/*READER*/}
          {(!!reader)&&<div style={{
              background: GRUVBOX.dark0,
              height: '100%',
              width:'100%',
              zIndex: 12,
              position:'fixed',
              top:0,
              left:0,
              display:'flex',
              flexDirection:'column',
              alignItems:'center'
          }}>
                         <div style={{
                             background: GRUVBOX.dark0_hard,
                             width:'calc(100% - 2rem)',
                             padding: '0.4rem 1rem',
                             height: '3rem',
                             display:'flex',
                             alignItems:'center',
                             justifyContent:'flex-end'
                         }}>
                           <p style={{marginRight:'auto'}}>{reader.name} ({reader.min})</p>
                           <IconButton style={{padding:'0.2rem'}} title={lang.GroupsView.reader.close||"Close"} onClick={()=>{
                               setReader(()=>null);
                           }} dimens={['2rem','2rem']} icon={<Feather.X/>} color={GRUVBOX.light2} hover={GRUVBOX.dark4}/></div>
                         {reader.products&&reader.products.map(({thumbnail, name=(lang.GroupsView.reader.unnamed||"Unnamed Product"), brand=(lang.ProductsView.nobrand||"Brand"), stock=1, id=""})=>{
                             return (
                                 <div style={StyleSheet.listItem}>
                                   {thumbnail && <img src={thumbnail} alt={name} style={StyleSheet.itemThumbnail}/>}
                                   {!thumbnail && <div style={StyleSheet.itemThumbnail}></div>}
                                   <div style={StyleSheet.itemText}>
                                     <p style={StyleSheet.itemName}>{brand}: {name} {stock>0?'('+stock+')':''}</p>
                                     <p style={StyleSheet.itemID}>{id}</p>
                                   </div>
                                   <div style={StyleSheet.itemButtons}>
                                     <IconButton title={lang.GroupsView.reader.opts.remove||"Remove"} onClick={async ()=>{
                                         if(window.confirm(format(lang.GroupsView.reader.remove.confirm||"Do you really want to remove {} from the group?",name))) {
                                             set(x=>{x.loading+=1; return x});
                                             const { error } = await state.supabase.client
                                                                          .from('Products')
                                                                          .upsert({...product_object[id], group:"none", id:id});
                                             set(x=>{x.loading-=1; return x});
                                             if(error) {
                                                 alert(lang.GroupsView.reader.remove.error||"Couldn't remove item from group")
                                             } else {
                                                 setReader(()=>null)
                                             }
                                         }
                                     }} dimens={['2rem', '2rem']} style={{padding:'0.4rem'}} icon={<Feather.Minus/>} hover={GRUVBOX.dark4} color={GRUVBOX.light2}/>

                                   </div>
                                 </div>
                             )
                         })}
                       </div>}
          {/*EDIT POPUP*/}
          <EditPopup state={state} edit={content.id?content.id.data:null} onCancel={()=>{
              edit_done();
              setContent(x=>{return {}});
          }} content={content}
                     set={setContent} onSave={async ()=>{
                         set(x=>{x.loading+=1; return x});
                         let payload = Object.fromEntries(Object.entries(content).map(([key, {data}])=>[key, data]));

                         const { error } = await state.supabase.client
                                                      .from('Groups')
                                                      .upsert(payload)
                         //                             .match({ id: content.id.data })
                         set(x=>{x.loading-=1; return x});
                         if(error) {
                             alert(lang.GroupsView.edit.error.save||"An error occured saving the group");
                         }
                         edit_done();
                         setContent(x=>{return {}});
                     }}/>

          {/*PRODUCTS LIST*/}
          <div style={StyleSheet.list}>
            {/*SEARCHBAR*/}
            <div style={{
                positon:'sticky',
                top:0,
                left: 0,
                width:'calc(100% - 4rem)',
                padding: '0.4rem',
                margin:'0.6rem 4rem',
                borderRadius: 12.5,
                height: '2rem',
                background:GRUVBOX.dark2,
                display:'flex',
                alignItems:'center'
            }}>
              <IconButton title={lang.GroupsView.saf.filter||"Filter"} dimens={['1.5rem', '1.5rem']} onClick={()=>barMode===1?setBarMode(0):setBarMode(1)} icon={<Feather.Filter/>} hover={GRUVBOX.dark4} color={GRUVBOX.light0} style={{padding:'0.4rem', margin:'0.2rem'}}/>
              <IconButton title={lang.GroupsView.saf.search||"Search"} dimens={['1.5rem', '1.5rem']} onClick={()=>barMode===2?setBarMode(0):setBarMode(2)} icon={<Feather.Search/>} hover={GRUVBOX.dark4} color={GRUVBOX.light0} style={{padding:'0.4rem', margin:'0.2rem'}}/>

              <div style={{
                  display:'flex',
                  flex: 1,
                  alignItems:'center',
              }}>
                {barMode===2&&<input style={{
                    flex: 1,
                    width:'100%',
                    background: GRUVBOX.dark1,
                    color: GRUVBOX.light1,
                    border:'none',
                    outline:'none',
                    padding: '0.6rem 0.4rem',
                    borderRadius: 12.5
                }} value={filter} placeholder={lang.GroupsView.saf.search||"SEARCH"} onChange={e=>setFilter(e.target.value)}/>}
                {barMode===1&&<div style={{
                    display:'flex',
                    flex: 1,
                    alignItems:'center',
                    justifyContent:'space-between'
                }}>
                                       <IconButton title={lang.GroupsView.saf.f.prev||"Previous"} icon={<Feather.ChevronLeft/>} dimens={['1rem','1rem']} color={GRUVBOX.light1} hover={GRUVBOX.dark1} onClick={()=>{
                                           if(sortMode===0) {
                                               setSortMode(SORT_MODE_NAMES.length-1);
                                           } else {
                                               setSortMode(sortMode-1);
                                           }
                                       }} style={{padding: '0.4rem'}}/>
                                       <p>{lang.GroupsView.saf.f.pre||"Sorting by: "}{SORT_MODE_NAMES[sortMode]}</p>
                                       <IconButton title={lang.GroupsView.saf.f.next||"Next"} icon={<Feather.ChevronRight/>} dimens={['1rem','1rem']} color={GRUVBOX.light1} hover={GRUVBOX.dark1} onClick={()=>{
                                           if(sortMode===SORT_MODE_NAMES.length-1) {
                                               setSortMode(0);
                                           } else {
                                               setSortMode(sortMode+1);
                                           }
                                       }} style={{padding: '0.4rem'}}/>
                                     </div>}
              </div>
              <IconButton title={lang.GroupsView.saf.clear||"Reset"} dimens={['1.5rem', '1.5rem']} onClick={()=>{setFilter(""); setSortMode(0)}} icon={<Feather.X/>} hover={GRUVBOX.dark4} color={GRUVBOX.light0} style={{padding:'0.4rem', margin:'0.2rem'}}/>
            </div>
            {groups.length===0&&<p>{lang.GroupsView.nogroups||"No Groups found"}</p>}
            {groups.map(({id="", priority=0, min=1, name="", color=GRUVBOX.faded_aqua, tags=[]})=>{
                let products = products_by_group[id] || [];

                if (products[0] && !name) name=products[0].brand+': '+products[0].name
                if(!name) name=lang.GroupsView.unnamed||'Unnamed Group'

                let counter = 0;
                products.forEach(({stock=0})=>{
                    counter+=stock;
                });
                let enough = 2;
                if(counter<min) {
                    enough = 0
                } else if(counter===min) {
                    enough = 1
                }
                let enough_text = counter+'/'+min;
                if(!color) color=GRUVBOX.faded_aqua;
                return (
                    <div style={{
                        ...StyleSheet.listItem,
                        opacity: (counter===0&&min===0)?0.6:1
                    }}>
                      {products.length!==1&&<div style={{
                          ...StyleSheet.itemThumbnail,
                          background: color
                      }}></div>}
                      {products.length===1&&<img style={StyleSheet.itemThumbnail} alt={name} src={products[0].thumbnail}/>}
                      <div style={StyleSheet.itemText}>
                        <p style={StyleSheet.itemName}>{name}<span style={{
                            color:[GRUVBOX.neutral_red, GRUVBOX.neutral_aqua, GRUVBOX.neutral_green][enough],
                            marginLeft:'0.4rem'
                        }}>     ({enough_text})</span></p>
                        <p style={StyleSheet.itemID}>{id}</p>
                      </div>
                      <div style={StyleSheet.itemButtons}>
                        {products.length>1&&<IconButton title={lang.GroupsView.opts.view||"View"} onClick={()=>{
                            setReader(()=>({
                                name: name,
                                min: min,
                                products: products
                            }));
                        }} dimens={['2rem', '2rem']} style={{padding:'0.4rem'}} icon={<Feather.Info/>} hover={GRUVBOX.dark4} color={GRUVBOX.light2}/>}
                        <IconButton title={lang.GroupsView.opts.edit||"Edit"} onClick={()=>{
                            // Open EDIT Dialog
                            state.groups.forEach(obj=>{
                                if(obj.id===id) {
                                    let d = {};
                                    let use = {
                                        name: "",
                                        color: "",
                                        priority: 1,
                                        min: 1,
                                        tags: []
                                    };
                                    Object.entries(obj).forEach(([key, value])=>{
                                        use[key]=value
                                    })
                                    Object.entries(use).forEach(([key, value])=>{
                                        d[key]={
                                            data: value||"",
                                            placeholder: ({})[key]||key,
                                            type: (["description"].includes(key))?'textarea':'input'
                                        }
                                    });
                                    setContent(()=>{
                                        return d;
                                    })
                                }
                            })
                        }} dimens={['2rem', '2rem']} style={{padding:'0.4rem'}} icon={<Feather.Edit2/>} hover={GRUVBOX.dark4} color={GRUVBOX.light2}/>
                        <IconButton title={lang.GroupsView.opts.delete||"Delete"} onClick={async ()=>{
                            // Delete
                            if (window.confirm(format(lang.GroupsView.delete.confirm||"Do you really want to delete >{}<?", name))) {
                                set(x=>{x.loading=x.loading+1; return x});
                                const {error} = await state.supabase.client
                                                           .from("Groups")
                                                           .delete()
                                                           .match({id: id});
                                set(x=>{x.loading=x.loading-1; return x});
                                if(error) {
                                    alert(lang.GroupsView.delete.error||"An Error occured")
                                }
                            }
                        }} dimens={['2rem', '2rem']} style={{padding:'0.4rem'}} icon={<Feather.Trash/>} hover={GRUVBOX.dark4} color={GRUVBOX.light2}/>
                      </div>
                    </div>
                );
            })}
          </div>
          {/*ADD BUTTON*/}

          <IconButton onClick={()=>{
              let d = {};
              Object.entries({
                  name:"",
                  tags:"{}",
                  color: "",
                  priority:1,
                  min: 1,
                  id: prompt(lang.GroupsView.add.prompt||"GroupID")
              }).forEach(([key, value])=>{
                  d[key]={
                      data: value||"",
                      placeholder: ({})[key]||key,
                      type: ([].includes(key))?'textarea':'input'
                  }
              });
              setContent(()=>{
                  return d;
              })
          }} dimens={['3rem','3rem']} icon={<Feather.Plus/>} background={GRUVBOX.faded_orange} hover={GRUVBOX.faded_yellow} color={GRUVBOX.light0} style={{
              position: 'fixed',
              right: 0,
              bottom: '5rem',
              padding:'0.4rem',
              display:'flex',
              alignItems:'center',
              justifyContent:'center',
              margin: '1rem',
              zIndex:8,
          }}/>

        </div>
    )
}
const StyleSheet = {
    list: {
        display:'flex',
        flexDirection:'column',
        alignItems:'center',
        overflowY:'auto'
    },
    listItem: {
        display:'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: 'calc(100% - 1rem - 5rem - 1rem)',
        margin: '0.4rem 0.5rem',
        padding: '0.4rem 1rem',
        alignItems:'center',
        justifyContent:'space-between',
        position: 'relative',
        paddingLeft: '5rem',
        background: GRUVBOX.dark0,
        borderRadius: 8
    },
    itemThumbnail: {
        borderRadius: '50%',
        background: GRUVBOX.faded_aqua,
        width: '2.5rem',
        height: '2.5rem',
        position: 'absolute',
        left: '1rem'
    },
    itemText: {
        display:'flex',
        flexDirection: 'column',
        alignItems:'flex-start',
        justifyContent:'center'
    },
    itemName: {
        fontWeight: 'bold',
        margin: 0,
    },
    itemID: {
        opacity: 0.5,
        fontSize:'0.8rem',
        margin: 0,
    },
    itemButtons: {
        display:'flex',
        flexWrap:'wrap',
        alignItems:'center',
        justifyContent:'center'
    }
}
