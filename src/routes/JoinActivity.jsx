/*
 * Activit launched if users has invite QR Code
 */
import * as React from 'react';
import * as ZXingBrowser from '@zxing/browser';
import * as Feather from 'react-feather';

import * as GRUVBOX from '../lib/gruvbox-colors';
import ModalBottomSheet from '../components/ModalBottomSheet';
import ConnectActivity from './ConnectActivity';
import Button from '../components/Button';
import Card from '../components/Card';
import IconButton from '../components/IconButton';
import {LanguageContext} from '../lib/translate';
import {format} from '../lib/rust';
import { useBigState } from '../lib/reactive-cloud';

export default function JoinActivity({updateRootView=()=>{}}) {

    const lang = React.useContext(LanguageContext);

    const [result, setResult] = React.useState(null);
    const vid = React.useRef();
    const codeReader = React.useRef(new ZXingBrowser.BrowserMultiFormatReader());
    const [at, st] = useBigState({
        cameras:[],
        cam: 0
    });
    React.useEffect(()=>{
        let controls = null;
        (async ()=>{ try {
            const videoInputDevices = await ZXingBrowser.BrowserCodeReader.listVideoInputDevices();
            st(x=>{x.cameras=videoInputDevices; return x});
            const selectedDeviceId = videoInputDevices[at.cam].deviceId;

            controls = await codeReader.current.decodeFromVideoDevice(selectedDeviceId, vid.current, (res, error, controls) => {
                if (!result && res) {
                    setResult(res);
                    try {
                        let json = JSON.parse(res.text);
                        if (json.from && json.SUPABASE_KEY && json.SUPABASE_URL) {
                            setModalOpt2(MODAL_OPT2_TEMPLATE.accept(json.SUPABASE_URL, json.SUPABASE_KEY));
                            setModalContent([
                                <p>{format(lang.JoinActivity.success.message||"Invite from {}",json.from)}</p>
                            ]);
                            setModalShown(true);
                        } else {
                            throw new Error(lang.JoinsActivity.failed.invalid||"No valid SUPABASE SHARE CODE");
                        }
                    } catch(e) {
                        setModalOpt2(MODAL_OPT2_TEMPLATE.tryharder());
                        setModalContent([
                            <p style={{
                                color: GRUVBOX.light2
                            }}> {lang.JoinActivity.failed.message||"An error occured"} </p>
                        ]);
                        setModalShown(true);
                    }
                }

            });
        }catch(e){}
                   })();

        return ()=>{
            controls?.stop();
        }
    }, [at.cam]);


    React.useEffect(()=>{

        let i = setInterval(()=>{
            if (!modalShown && !result) {
                setModalOpt2(MODAL_OPT2_TEMPLATE.tryharder());
                setModalContent([
                    <p style={{
                        color: GRUVBOX.light2
                    }}> {lang.JoinActivity.failed.message||"An error occurred"} </p>
                ]);
                setModalShown(true);
            }
        }, 1000*20);
        return ()=>{
            clearInterval(i);
        }
    }, [])


    let MODAL_OPT2_TEMPLATE = {
        tryharder: ()=>[lang.JoinActivity.failed.tryharder||"Try harder", ()=>{
            setResult(null);
            setModalShown(false);
        }, GRUVBOX.faded_blue],
        accept: (url, key)=>[lang.JoinActivity.success.accept||"Accept", ()=>updateRootView(<ConnectActivity SUPABASE_URL={url} SUPABASE_KEY={key} back_action={()=>updateRootView(<JoinActivity />)}/>), GRUVBOX.faded_green]
    }
    const [modal_opt2, setModalOpt2] = React.useState(MODAL_OPT2_TEMPLATE.tryharder());
    const [modalShown, setModalShown] = React.useState(false);
    const [modalContent, setModalContent] = React.useState("");

    return (
        <div style={{
            background: GRUVBOX.dark0_hard,
            height: '100%',
            width: '100%',
            display: 'flex',
            flexDirection: 'column',
            flexWrap: 'wrap',
            alignItems: 'center',
            justifyContent: 'space-evenly',
            color: GRUVBOX.light0,
            overflowY:'auto'
        }}>
                <Card background={GRUVBOX.dark1} style={{
        borderRadius:12.5,
        margin: '2rem',
        maxWidth:'75vmin',
        minWidth:'45vmin',
        display:'flex',
        flexDirection:'column'
      }} children={[
        <video ref={vid}></video>,
        <div style={{display:'flex'}}>
          {at.cameras&&at.cam!==0&&<IconButton dimens={['2rem', '2rem']} icon={<Feather.ChevronLeft/>} title={lang.ProductScanView.camera.prev} onClick={()=>{
          st(x=>{x.cam-=1; return x});
        }} style={{padding:'0.4rem'}} color={GRUVBOX.light1} hover={GRUVBOX.dark2}/>}
          {at.cameras&&at.cam!==at.cameras.length-1&&<IconButton dimens={['2rem','2rem']} icon={<Feather.ChevronRight/>} title={lang.ProductScanView.camera.next} onClick={()=>{
          st(x=>{x.cam+=1; return x});
        }} style={{padding:'0.4rem'}} color={GRUVBOX.light1} hover={GRUVBOX.dark2}/>}
        </div>
      ]}/>
          <Button style={StyleSheet.buttons} text={lang.JoinActivity.back} onClick={()=>window.location.reload()} background="transparent" hover={GRUVBOX.faded_orange} color={GRUVBOX.light2}/>
          {/*BOTTOM Sheet*/}
          <ModalBottomSheet shadow={GRUVBOX.dark0_hard} show={modalShown} background={GRUVBOX.dark1} buttons={[
              ["Cancel", ()=>window.location.reload(), GRUVBOX.faded_orange, GRUVBOX.light1],
              [modal_opt2[0], modal_opt2[1], modal_opt2[2], GRUVBOX.light1]
          ]} children={modalContent}/>
        </div>
    );
};

const StyleSheet = {

}
