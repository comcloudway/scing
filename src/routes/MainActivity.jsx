/*
 * Activity launched once user is logged in / connected
 */
import * as React from 'react';
import * as Feather from 'react-feather';
import { createClient } from '@supabase/supabase-js';
import { useDrag } from 'react-use-gesture'

import * as GRUVBOX from '../lib/gruvbox-colors';
import BottomNavigationBar, {createBottomNavigationBarItem} from '../components/BottomNavigationBar';
import Loader from '../components/Loader';
import {useBigState} from '../lib/reactive-cloud';
import {LanguageContext} from '../lib/translate';

import ProductScanView from './ProductScanView';
import ProductsView from './ProductsView';
import RecipiesView from './RecipiesView';
import GroupsView from './GroupsView';
import ToolsView from './ToolsView';

export default function MainActivity() {
    const lang = React.useContext(LanguageContext);

    const[state, set] = useBigState({
        // CONSTANTS
        user: {
            name: localStorage.getItem("USER"),
        },
        supabase: {
            url: localStorage.getItem("SUPABASE_URL"),
            key: localStorage.getItem("SUPABASE_KEY"),
            client: null
        },

        // UI
        loading: false,


        // DATABASE ENTRIES
        products: [],
        recipes: [],
        groups: []
    });

    const [manu_view, setManuView] = React.useState(null);
    const CONFIG = {
        scan: [Feather.Maximize, lang.MainActivity.categories.scan, <ProductScanView state={state} set={set} updateFlipper={v=>{
            setManuView(v);
        }}/>],
        groups: [Feather.ShoppingCart, lang.MainActivity.categories.groups, <GroupsView state={state} set={set}/>],
        products: [Feather.Archive, lang.MainActivity.categories.products, <ProductsView state={state} set={set}/>],
        recipes: [Feather.Book, lang.MainActivity.categories.recipes, <RecipiesView state={state} set={set}/>],
        tools: [Feather.Tool, lang.MainActivity.categories.tools, <ToolsView state={state} set={set}/>]
    };

    const BNB_CONFIG = Object.entries(CONFIG).map(([key, [icon, text]])=>{
        return createBottomNavigationBarItem(key, icon, text)
    })

    const [index, setIndex] = React.useState(1);

    React.useEffect(()=>{
        let {href, origin} = window.location;
        let route = href.slice(origin.length);
        if(route.startsWith("/")) route=route.slice(1);
        let routes = {
            scan: 0,
            groups: 1,
            products: 2,
            recipes: 3,
            tools: 4
        };
        if (routes[route]) setIndex(routes[route]);
    }, []);

    const TOOLS = {
        fetchTable: (supabase, table, select="*") => new Promise(async (res, rej)=>{
            try {
                let { data, error } = await supabase
                    .from(table)
                    .select(select);
                if (error) {
                    rej(error)
                } else {
                    res(data)
                }
            } catch(e) {
                rej(e)
            }
        })
    }

    React.useEffect(()=>{
        const supabase = createClient(state.supabase.url, state.supabase.key);
        set(x=>{x.supabase.client = supabase; return x});

        let tables = ['products', 'groups', 'recipes'];
        let subs = [];

        // prefetch tables
        tables.forEach(id=>{
            let table = id[0].toUpperCase()+id.slice(1);
            set(x=>{x.loading=x.loading+1; return x});
            TOOLS.fetchTable(supabase, table)
                 .then(dt=>{
                     set(x=>{x.loading=x.loading-1; return x});
                     set(x=>{x[id]=dt; return x});
                     let listener = supabase
                         .from(table)
                         .on('*', payload=>{
                             // maybe do sth
                             TOOLS.fetchTable(supabase, table)
                                  .then(dt=>{
                                      set(x=>{x[id]=dt; return x});
                                  })
                                  .catch(()=>{
                                  })
                         })
                         .subscribe();
                     subs.push(listener)
                 })
                 .catch(()=>{
                     set(x=>{x.loading=x.loading-1; return x});
                 })

        });

        return ()=>{
            // unsubscribe all listeners
            subs.forEach(sub=>supabase.removeSubscription(sub));
        }
    }, []);

    const [bright, setBright] = React.useState(0);
    const bind = useDrag(({down, movement:[x]})=>{
        if(down) setBright(x*2/window.innerWidth*100);
        if(!down) setBright(0);
        if(!down&&x>=window.innerWidth/2-20) {
        let tables = ['products', 'groups', 'recipes'];
        tables.forEach(id=>{
            let table = id[0].toUpperCase()+id.slice(1);
            set(x=>{x.loading=x.loading+1; return x});
            TOOLS.fetchTable(state.supabase.client, table)
                 .then(dt=>{
                     set(x=>{x.loading=x.loading-1; return x});
                     set(x=>{x[id]=dt; return x});
                 })
                 .catch(e=>{
                     set(x=>{x.loading=x.loading-1; return x});
                 })
        });
        }
    })

    return (
        <div style={{
            background: GRUVBOX.dark0_hard,
            height: '100%',
            width: '100%',
            display: 'flex',
            flexDirection: 'column',
            flexWrap: 'wrap',
            alignItems: 'center',
            justifyContent: 'space-evenly',
            color: GRUVBOX.light0,
            overflowY:'auto'
        }}>
          <div style={{
              height: 'calc(100% - 5rem)',
              width: '100%',
              position: 'absolute',
              left: 0,
              top: 0,
              overflowY:'auto'
          }}>{manu_view?manu_view:Object.values(CONFIG)[index][2]}</div>
          <BottomNavigationBar {...bind()} style={{userSelect:'none',backgroundImage:`linear-gradient(90deg, ${GRUVBOX.dark4} ${bright}%, ${GRUVBOX.dark1} ${bright}%, ${GRUVBOX.dark1} ${100-bright}%)`}} index={index} onClick={key=>{
              setManuView(null);
              setIndex(Object.keys(CONFIG).indexOf(key)||0);
          }} children={BNB_CONFIG} theme={[GRUVBOX.light2, GRUVBOX.neutral_aqua]} background={GRUVBOX.dark1}/>

          {state.loading>0&&<Loader onContextMenu={e=>{
              e.preventDefault();
              if(window.confirm(lang.MainActivity.bored||"Do you feel your request is taking to long? - Want to log out?")) {
                  localStorage.clear();
                  window.location.reload();
              }
          }} show={state.loading>0} background={GRUVBOX.dark0_hard} color={GRUVBOX.light0} style={{zIndex:20}}/>}
        </div>
    );
}
