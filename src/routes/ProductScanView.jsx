import React from 'react';
import * as ZXingBrowser from '@zxing/browser';

import * as GRUVBOX from '../lib/gruvbox-colors';
import Button from '../components/Button';
import ProductsView from './ProductsView';
import Card from '../components/Card';
import {LanguageContext} from '../lib/translate';
import IconButton from '../components/IconButton';
import * as Feather from 'react-feather';
import { useBigState } from '../lib/reactive-cloud';

export default function ProductScanView({state={}, set=()=>{}, updateFlipper=()=>{}}) {
  const [result, setResult] = React.useState(null);
  const lang = React.useContext(LanguageContext);

  const product_object = React.useMemo(()=>{
    let o = {};

    state.products.forEach(obj=>{
      o[obj.id]={...obj};
      delete o[obj.id].id
    });

    return o;
  }, [state.products])

  const vid = React.useRef();
  const codeReader = React.useRef(new ZXingBrowser.BrowserMultiFormatReader());
  const [{cam, cameras}, st] = useBigState({
    cam: 0,
    cameras: []
  })
  React.useEffect(()=>{
    let controls = null;
    (async()=>{
      try {
        const videoInputDevices = await ZXingBrowser.BrowserCodeReader.listVideoInputDevices();
        st(x=>{x.cameras=videoInputDevices; return x});

        const selectedDeviceId = videoInputDevices[cam].deviceId;

        controls = await codeReader.current.decodeFromVideoDevice(selectedDeviceId, vid.current, (res, error, ctrl) => {
          if (!result && res) {
            setResult(res);
          }
          controls = ctrl
        });
      }catch(e){}
    })();
    return ()=>{
      controls?.stop();
    }
  }, [cam]);

  return (
    <div style={{
      display:'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      alignItems:'center',
      justifyContent:'center',
      minHeight:'100%'
    }}>
      <Card background={GRUVBOX.dark1} style={{
        borderRadius:12.5,
        margin: '2rem',
        maxWidth:'75vmin',
        minWidth:'45vmin',
        display:'flex',
        flexDirection:'column'
      }} children={[
        <video ref={vid}></video>,
        <div style={{display:'flex'}}>
        {cameras&&cam!==0&&<IconButton dimens={['2rem', '2rem']} icon={<Feather.ChevronLeft/>} title={lang.ProductScanView.camera.prev} onClick={()=>{
          st(x=>{x.cam-=1; return x});
        }} style={{padding:'0.4rem'}} color={GRUVBOX.light1} hover={GRUVBOX.dark2}/>}
        {cameras&&cam!==cameras.length-1&&<IconButton dimens={['2rem','2rem']} icon={<Feather.ChevronRight/>} title={lang.ProductScanView.camera.next} onClick={()=>{
          st(x=>{x.cam+=1; return x});
        }} style={{padding:'0.4rem'}} color={GRUVBOX.light1} hover={GRUVBOX.dark2}/>}
        </div>
      ]}/>

      <div>
        <input style={{
          background: GRUVBOX.dark0,
          padding: '0.8rem',
          margin: '1rem',
          borderRadius: 8,
          border:'none',
          borderLeft: '2rem solid ' + GRUVBOX.faded_aqua,
          color: GRUVBOX.light2,
          outline:'none'
        }} onChange={e=>setResult({text:e.target.value})} value={result?.text||""} placeholder={lang.ProductScanView.code||"Code"}/>
      </div>

      <div>
        {!!result&&<Button text={lang.ProductScanView.tryharder||"Try harder"} onClick={()=>{
          setResult(null);
        }} color={GRUVBOX.light1} background="transparent" hover={GRUVBOX.faded_red}/>}

        {!!result&&result.text&&!product_object[result.text.trim()]&&<Button text={lang.ProductScanView.createproduct||"Add Product"} onClick={async ()=>{
          // add product + change screen
          set(x=>{x.loading+=1; return x});
          state.products.forEach(({id})=>{
            if(result.text.trim()===id) {
              window.alert(lang.ProductScanView.errors.createproductexists||"Product already exists");
              return;
            }
          });
          const { error } = await state.supabase.client
                                       .from('Products')
                                       .insert([
                                         { id: result.text.trim(), name: "", brand:""}
                                       ]);
          if(error) {
            alert(lang.ProductScanView.errors.createproducterror||"Couldn't create product")
          } else {
            let d = {
            };
            Object.entries({
              id:result.text.trim(),
              name:"",
              brand:"",
              tags:"{}",
              group:"none",
              thumbnail:null,
              stock:1
            }).forEach(([key,value])=>{
              d[key]={
                data:value,
                placeholder: ({})[key]||key,
                type: (["description"].includes(key))?'textarea':'input'
              }
            });
            d.id.data=result.text.trim();
            updateFlipper(<ProductsView edit_data={d} set={set} state={state} edit_done={()=>{
              updateFlipper(<ProductScanView set={set} state={state} updateFlipper={updateFlipper}/>)
            }}/>)
          }
          set(x=>{x.loading-=1; return x});
        }} color={GRUVBOX.light1} background="transparent" hover={GRUVBOX.faded_green}/>}

        {!!result&&result.text&&!!product_object[result.text.trim()]&&<Button text={lang.ProductScanView.addtostock||"Add to Stock"} onClick={async ()=>{
          // add X to stock
          let id = result.text.trim()
          let m = 1;
          try {
            let i = prompt(lang.ProductScanView.addtostockamount||"Amount",m)
            if(!i) return;
            m=parseInt(i);
          } catch(e) {
            m=1
          }
          product_object[id].stock+=m;
          set(x=>{x.loading+=1; return x});
          const { error } = await state.supabase.client
                                       .from('Products')
                                       .upsert({ id: id, ...product_object[id] })
          set(x=>{x.loading-=1; return x});
          if(error) {
            alert(lang.ProductScanView.errors.addtostockerror||"Could not add Item to Stock pile")
          } else {
            alert(lang.ProductScanView.success.addedtostock||"Successfully added Item to Stock")
          }
        }} color={GRUVBOX.light1} background="transparent" hover={GRUVBOX.faded_aqua}/>}
        {!!result&&result.text&&product_object[result.text.trim()]&&product_object[result.text.trim()].stock>0&&<Button text={lang.ProductScanView.delfromstock||"Remove from Stock"} onClick={async ()=>{
          // check if in stock
          let id = result.text.trim();
          if(product_object[id].stock>0) {
            product_object[id].stock-=1;
            // remove X from stock
            set(x=>{x.loading+=1; return x});
            const { error } = await state.supabase.client
                                         .from('Products')
                                         .update(product_object[id])
                                         .match({ id: id })
            set(x=>{x.loading-=1; return x});
            if(error) {
              alert(lang.ProductScanView.errors.delfromstockerror||"An error occured removing the product");
            } else {
              alert(lang.ProductScanView.success.delfromstock||"Successfully removed 1 from Stock");
            }
          } else {
            alert(lang.ProductScanView.errors.delfromstocknothingleft||"Item not in Stock")
          }
        }} color={GRUVBOX.light1} background="transparent" hover={GRUVBOX.faded_blue}/>}
      </div>

    </div>
  );
}
