import * as React from 'react';
import * as Feather from 'react-feather';

import * as GRUVBOX from '../lib/gruvbox-colors';
import IconButton from '../components/IconButton';
import EditPopup from '../components/EditPopup';
import {useBigState} from '../lib/reactive-cloud';
import {LanguageContext} from '../lib/translate';
import {format} from '../lib/rust';

export default function ProductsView({state={}, set=()=>{}, edit_data={id:""}, edit_done=()=>{}}) {
    const [content, setContent] = useBigState(edit_data);

    const lang = React.useContext(LanguageContext);
    const product_object = React.useMemo(()=>{

        let o = {};

        state.products.forEach(obj=>{
            o[obj.id]={...obj};
            delete o[obj.id].id
        });

        return o;
    }, [state.products])

    const groups_by_id = React.useMemo(()=>{
        let o = {};

        state.groups.forEach(obj=>{
            o[obj.id]={...obj};
            delete o[obj.id].id
        });
        return o;
    }, [state.groups])

    const products_by_group = React.useMemo(()=>{
        let s = {};
        state.products.forEach(obj=>{
            let {group=""} = obj;
            if(!s[group]) s[group]=[];
            s[group].push(obj)
        });
        return s
    }, [state.products])

    const [filter, setFilter] = React.useState("");
    const [sortMode, setSortMode] = React.useState(0);
    const [barMode, setBarMode] = React.useState(0);

    const SORT_FUNCTIONS = [
        // A -> Z
        (arr=[])=>{
            return arr.sort((a,b)=>{
                let oa = product_object[a.id];
                let ob = product_object[b.id];
                let na = "Unknown Product";
                let nb = "Unknown Product";
                if(oa) na=oa.name;
                if(ob) nb=ob.name;
                na=na.toLowerCase();
                nb=nb.toLowerCase();
                if(na===nb) return 0;
                let arr = [na,nb].sort();
                if (arr[0]===na) return -1;
                return 1;
            });
        },
        // Z -> A
        (arr=[])=>{
            return SORT_FUNCTIONS[0](arr).reverse()
        },
        // availability H->L
        (arr=[])=>arr.sort((a,b)=>{
            let oa = product_object[a.id];
            let ob = product_object[b.id];
            let sa = 0;
            let sb = 0;
            if(oa&&oa.stock)sa=oa.stock;
            if(ob&&ob.stock)sb=ob.stock;
            if(sa===sb) return 0;
            if(sa<sb) return -1;
            return 1;
        }),
        (arr=[])=>SORT_FUNCTIONS[2](arr).reverse()
    ];
    const SORT_MODE_NAMES = lang.ProductsView.saf.sortmodes||['Alphabet A->Z', 'Alphabet Z->A', 'Availability H->L', 'Availability L->H']

    const products = React.useMemo(()=>{
        let s = state.products.filter(obj=>{
            if((product_object[obj.id] && ((product_object[obj.id].brand.toLowerCase().includes(filter.toLowerCase()))||(filter.toLowerCase().includes(product_object[obj.id].brand.toLowerCase()))))||
               (product_object[obj.id] && (product_object[obj.id].name.toLowerCase().includes(filter.toLowerCase())||filter.toLowerCase().includes(product_object[obj.id].name.toLowerCase())))||(obj.tags && obj.tags.filter(t=>t.toLowerCase().includes(filter.toLowerCase())||filter.toLowerCase().includes(t.toLowerCase())).length>0)) return true
            return false;
        });
        let sf = SORT_FUNCTIONS[sortMode];
        if(!sf) sf=SORT_FUNCTIONS[0];
        s = sf(s);
        return s;
    }, [state.products,filter, product_object, sortMode]);


    return (
        <div>
          {/*EDIT POPUP*/}
          <EditPopup state={state} enogroup={lang.ProductsView.edit.enogroup} edit={content.id?content.id.data:null} onCancel={()=>{
              edit_done();
              setContent(x=>{return {}});
          }} content={content}
                     set={setContent} onSave={async ()=>{
                         set(x=>{x.loading+=1; return x});
                         let payload = Object.fromEntries(Object.entries(content).filter(v=>v[0]!=="id").map(([key, {data}])=>[key, data]));
                         delete payload.id
                         const { error } = await state.supabase.client
                                                      .from('Products')
                                                      .update(payload)
                                                      .match({ id: content.id.data })
                         set(x=>{x.loading-=1; return x});
                         if(error) {
                             alert(lang.ProductsView.edit.error||"An error occured saving the product");
                         }
                         edit_done();
                         setContent(x=>{return {}});
                     }}/>
          {/*PRODUCTS LIST*/}
          <div style={StyleSheet.list}>
            {/*SEARCHBAR*/}
            <div style={{
                positon:'sticky',
                top:0,
                left: 0,
                width:'calc(100% - 4rem)',
                padding: '0.4rem',
                margin:'0.6rem 4rem',
                borderRadius: 12.5,
                height: '2rem',
                background:GRUVBOX.dark2,
                display:'flex',
                alignItems:'center'
            }}>
              <IconButton title={lang.ProductsView.saf.filter||"Filter"} dimens={['1.5rem', '1.5rem']} onClick={()=>barMode===1?setBarMode(0):setBarMode(1)} icon={<Feather.Filter/>} hover={GRUVBOX.dark4} color={GRUVBOX.light0} style={{padding:'0.4rem', margin:'0.2rem'}}/>
              <IconButton title={lang.ProductsView.saf.search||"Search"} dimens={['1.5rem', '1.5rem']} onClick={()=>barMode===2?setBarMode(0):setBarMode(2)} icon={<Feather.Search/>} hover={GRUVBOX.dark4} color={GRUVBOX.light0} style={{padding:'0.4rem', margin:'0.2rem'}}/>

              <div style={{
                  display:'flex',
                  flex: 1,
                  alignItems:'center',
              }}>
                {barMode===2&&<input style={{
                    flex: 1,
                    width:'100%',
                    background: GRUVBOX.dark1,
                    color: GRUVBOX.light1,
                    border:'none',
                    outline:'none',
                    padding: '0.6rem 0.4rem',
                    borderRadius: 12.5
                }} value={filter} placeholder={lang.ProductsView.saf.search||"SEARCH"} onChange={e=>setFilter(e.target.value)}/>}
                {barMode===1&&<div style={{
                    display:'flex',
                    flex: 1,
                    alignItems:'center',
                    justifyContent:'space-between'
                }}>
                                                                                           <IconButton title={lang.ProductsView.saf.f.prev||"Previous"} icon={<Feather.ChevronLeft />} dimens={['1rem','1rem']} color={GRUVBOX.light1} hover={GRUVBOX.dark1} onClick={()=>{
                                                                                               if(sortMode===0) {
                                                                                                   setSortMode(SORT_MODE_NAMES.length-1);
                                                                                               } else {
                                                                                                   setSortMode(sortMode-1);
                                                                                               }
                                                                                           }} style={{padding: '0.4rem'}}/>
                                                                                           <p>{lang.ProductsView.saf.f.pre||"Sorting by: "}{SORT_MODE_NAMES[sortMode]}</p>
                                                                                           <IconButton title={lang.ProductsView.saf.f.next||"Next"} icon={<Feather.ChevronRight/>} dimens={['1rem','1rem']} color={GRUVBOX.light1} hover={GRUVBOX.dark1} onClick={()=>{
                                                                                               if(sortMode===SORT_MODE_NAMES.length-1) {
                                                                                                   setSortMode(0);
                                                                                               } else {
                                                                                                   setSortMode(sortMode+1);
                                                                                               }
                                                                                           }} style={{padding: '0.4rem'}}/>
                                                                                         </div>}
              </div>
              <IconButton title={lang.ProductsView.saf.clear||"Clear"} dimens={['1.5rem', '1.5rem']} onClick={()=>{setFilter(""); setSortMode(0)}} icon={<Feather.X/>} hover={GRUVBOX.dark4} color={GRUVBOX.light0} style={{padding:'0.4rem', margin:'0.2rem'}}/>
            </div>
            {products.length===0&&<p>{lang.ProductsView.noproducts||"No Products"}</p>}
            {products.map(({id="", stock=0, name=(lang.ProductsView.unnamed||"Unknown Product"), tags=[], thumbnail=null, brand="No brand",group="none"})=>{
                return (
                    <div style={StyleSheet.listItem}>
                      {thumbnail && <img src={thumbnail} alt={name} style={StyleSheet.itemThumbnail}/>}
                      {!thumbnail && <div style={StyleSheet.itemThumbnail}></div>}
                      <div style={StyleSheet.itemText}>
                        <p style={StyleSheet.itemName}>{brand}: {name} {stock>0?'('+stock+')':''}</p>
                        <p style={StyleSheet.itemID}>{id}</p>
                      </div>
                      <div style={StyleSheet.itemButtons}>
                        {(group==="none"||!groups_by_id[group])&&<IconButton title={lang.ProductsView.opts.group||"Group"} onClick={async ()=>{
                            // Group
                            if (window.confirm(format(lang.ProductsView.group.confirm||'Do you really want to group/order >{}<?',name))) {
                                set(x=>{x.loading=x.loading+1; return x});
                                let g = {};
                                g={min:1,priority:1,color:"",tags:[]};
                                if(groups_by_id[id]) g = groups_by_id[id];
                                g.id=id;
                                const {error} = await state.supabase.client
                                                           .from("Groups")
                                                           .upsert(g)
                                set(x=>{x.loading=x.loading-1; return x});
                                if(error) {
                                    alert(lang.ProductsView.group.error||"An Error occured")
                                } else {
                                 set(x=>{x.loading=x.loading+1; return x});
                                 const {error} = await state.supabase.client
                                                           .from("Products")
                                                           .update({id:id, group:id})
                                                           .match({id:id})
                                set(x=>{x.loading=x.loading-1; return x});
                                if(error) {
                                    alert(lang.ProductsView.ungroup.error||"An Error occured")
                                }
                                }
                            }
                        }} dimens={['2rem', '2rem']} style={{padding:'0.4rem'}} icon={<Feather.FolderPlus/>} hover={GRUVBOX.dark4} color={GRUVBOX.light2}/>}
                        {group===id&&groups_by_id[id]&&products_by_group[id]&&products_by_group[id].length===1&&<IconButton title={lang.ProductsView.opts.ungroup||"Ungroup"} onClick={async ()=>{
                            // Uungroup
                            if (window.confirm(format(lang.ProductsView.ungroup.confirm||'Do you really want to ungroup >{}<?',name))) {
                                set(x=>{x.loading=x.loading+1; return x});
                                 const {error} = await state.supabase.client
                                                           .from("Products")
                                                           .update({id:id, group:"none"})
                                                           .match({id:id})
                                set(x=>{x.loading=x.loading-1; return x});
                                if(error) {
                                    alert(lang.ProductsView.ungroup.error||"An Error occured")
                                } else {
                                set(x=>{x.loading=x.loading+1; return x});
                                const {error} = await state.supabase.client
                                                           .from("Groups")
                                                           .delete()
                                                           .match({id:id})
                                set(x=>{x.loading=x.loading-1; return x});
                                if(error) {
                                    alert(lang.ProductsView.ungroup.error||"An Error occured")
                                }
                                }
                            }
                        }} dimens={['2rem', '2rem']} style={{padding:'0.4rem'}} icon={<Feather.FolderMinus/>} hover={GRUVBOX.dark4} color={GRUVBOX.light2}/>}
                        <IconButton title={lang.ProductsView.opts.edit||"Edit"} onClick={()=>{
                            // Open EDIT Dialog
                            state.products.forEach(obj=>{
                                if(obj.id===id) {
                                    let d = {};
                                    Object.entries(obj).forEach(([key, value])=>{
                                        d[key]={
                                            data: value||"",
                                            placeholder: ({})[key]||key,
                                            type: (["description"].includes(key))?'textarea':'input'
                                        }
                                    });
                                    setContent(()=>{
                                        return d;
                                    })
                                }
                            })
                        }} dimens={['2rem', '2rem']} style={{padding:'0.4rem'}} icon={<Feather.Edit2/>} hover={GRUVBOX.dark4} color={GRUVBOX.light2}/>
                        <IconButton title={lang.ProductsView.opts.delete||"Delete"} onClick={async ()=>{
                            // Delete
                            if (window.confirm(format(lang.ProductsView.delete.confirm||'Do you really want to delete >{}<?',name))) {
                                set(x=>{x.loading=x.loading+1; return x});
                                const {error} = await state.supabase.client
                                                           .from("Products")
                                                           .delete()
                                                           .match({id: id});
                                set(x=>{x.loading=x.loading-1; return x});
                                if(error) {
                                    alert(lang.ProductsView.delete.error||"An Error occured")
                                }
                            }
                        }} dimens={['2rem', '2rem']} style={{padding:'0.4rem'}} icon={<Feather.Trash/>} hover={GRUVBOX.dark4} color={GRUVBOX.light2}/>

                      </div>
                    </div>
                );
            })}
          </div>
        </div>
    )
}
const StyleSheet = {
    list: {
        display:'flex',
        flexDirection:'column',
        alignItems:'center',
        overflowY:'auto'
    },
    listItem: {
        display:'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: 'calc(100% - 1rem - 5rem - 1rem)',
        margin: '0.4rem 0.5rem',
        padding: '0.4rem 1rem',
        alignItems:'center',
        justifyContent:'space-between',
        position: 'relative',
        paddingLeft: '5rem',
        background: GRUVBOX.dark0,
        borderRadius: 8
    },
    itemThumbnail: {
        borderRadius: '50%',
        background: GRUVBOX.faded_aqua,
        width: '2.5rem',
        height: '2.5rem',
        position: 'absolute',
        left: '1rem'
    },
    itemText: {
        display:'flex',
        flexDirection: 'column',
        alignItems:'flex-start',
        justifyContent:'center'
    },
    itemName: {
        fontWeight: 'bold',
        margin: 0,
    },
    itemID: {
        opacity: 0.5,
        fontSize:'0.8rem',
        margin: 0,
    },
    itemButtons: {
        display:'flex',
        flexWrap:'wrap',
        alignItems:'center',
        justifyContent:'center'
    }
}
