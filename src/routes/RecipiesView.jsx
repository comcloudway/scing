import * as React from 'react';
import * as Feather from 'react-feather';

import * as GRUVBOX from '../lib/gruvbox-colors';
import IconButton from '../components/IconButton';
import EditPopup from '../components/EditPopup';
import {useBigState} from '../lib/reactive-cloud';
import {LanguageContext} from '../lib/translate';
import {format} from '../lib/rust';

export default function RecipiesView({state={}, set=()=>{}, edit_data={id:""}, edit_done=()=>{}}) {
    const [content, setContent] = useBigState(edit_data);
    const lang = React.useContext(LanguageContext);

    const products_by_group = React.useMemo(()=>{
        let s = {};
        state.products.forEach(obj=>{
            let {group=""} = obj;
            if(!s[group]) s[group]=[];
            s[group].push(obj)
        });
        return s
    }, [state.products])

    const recipies_object = React.useMemo(()=>{
        let o = {};
        state.recipes.forEach(obj=>{
            o[obj.id]={...obj};
            delete o[obj.id].id
        });
        return o;
    }, [state.recipes]);
    const groups_object = React.useMemo(()=>{
        let o = {};
        state.groups.forEach(obj=>{
            o[obj.id]={...obj};
            delete o[obj.id].id
        });
        return o;
    }, [state.groups]);

    const products_object = React.useMemo(()=>{
        let o = {};
        state.products.forEach(obj=>{
            o[obj.id]={...obj};
            delete o[obj.id].id
        });
        return o;
    }, [state.products]);

    const [filter, setFilter] = React.useState("");
    const [sortMode, setSortMode] = React.useState(0);
    const [barMode, setBarMode] = React.useState(0);

    const SORT_FUNCTIONS = [
        (arr=[])=>arr.sort((a,b)=>{
            let na = a.name || lang.RecipeView.unnamed || "Unnamed Recipe"
            let nb = b.name || lang.RecipeView.unnamed || "Unnamed Recipe"
            na=na.toLowerCase();
            nb=nb.toLowerCase();
            if (na===nb) return 0;
            let arr= [na,nb].sort();
            if(arr[0]===na) return -1;
            return 1;
        }),
        (arr=[])=>SORT_FUNCTIONS[0](arr).reverse(),
        (arr=[])=>arr.sort((a,b)=>{
            let [ha,hb] = [a,b].map(obj=>{
                let json = {};
                try {
                    json = JSON.parse(obj)
                } catch(e) {
                    json={}
                }
                let o = Object.entries(json);
                let c = 0;
                o.forEach(([id, amount])=>{
                    let has = 0;
                    if(products_object[id]) {
                        has=products_object[id].stock||0;
                    } else if (products_by_group[id]) {
                        products_by_group[id].forEach(({stock})=>has+=stock)
                    }
                    c+=has/amount;
                })
                return c/o.length
            });

            if (ha === hb) return 0;
            if (ha<hb) return -1;
            if (ha>hb) return 1;
        }),
        (arr=[])=>SORT_FUNCTIONS[2](arr).reverse()
    ];
    const SORT_MODE_NAMES = lang.RecipeView.saf.sortmodes||['Alphabet A->Z', 'Alphabet Z->A', 'Availability H->L', 'Availability L->H'];

    const recipies = React.useMemo(()=>{
        let s = state.recipes.filter(obj=>{
            if(!filter) return true;
            if(obj.name.toLowerCase().includes(filter.toLowerCase())||
               filter.toLowerCase().includes(obj.name.toLowerCase())||
               (obj.tags && Array.isArray(obj.tags) &&
                obj.tags.filter(t=>t.toLowerCase().includes(filter.toLowerCase())||
                                filter.toLowerCase().includes(t.toLowerCase())).length>0)) return true
            return false;
        });
        let sf = SORT_FUNCTIONS[sortMode];
        if(!sf) sf=SORT_FUNCTIONS[0];
        s = sf(s);
        return s;
    }, [state.recipes,filter, sortMode]);


    const [reader, setReader] = useBigState(null);

    return (
        <div>
          {/*READER POPUP*/}
          {reader&&<div style={{
              height:'100%',
              width:'100%',
              position:'fixed',
              top: 0,
              left: 0,
              background: GRUVBOX.dark1,
              zIndex:12,
              overflowY:'auto'
          }}>
                                              {reader.index===0&&<div style={{
                                                  display:'flex',
                                                  flexDirection:'column',
                                                  alignItems:'center',
                                                  overflowY:'auto',
                                                  height: 'calc(100% - 5rem)'
                                              }}>
             {/*NAME & THUMBNAIL & TAGS*/}
             {reader.thumbnail&&<img style={{
                 borderRadius:25,
                 margin: '1rem'
             }} src={reader.thumbnail} alt={reader.name||lang.RecipeView.unnamed||"A recipie"}/>}
             <p style={{
                 fontWeight:'bold',
                 margin: '1rem'
             }}>{reader.name}</p>
                                                                   {reader.tags&&Array.isArray(reader.tags)&&<div style={{
                 display:'flex',
                 flexWrap:'wrap'
             }}>
               {reader.tags.map(tag=>{
                   return (<div style={{
                       background: GRUVBOX.faded_orange,
                       padding:'0.4rem 0.8rem',
                       borderRadius:'25px',
                       margin:'1rem'
                   }}>{tag}</div>)
               })}
             </div>}
           </div>}
                                              {reader.index===1&&<div style={{
                                                  display:'flex',
                                                  flexDirection:'column',
                                                  alignItems:'center',
                                                  height: 'calc(100% - 5rem)',
                                                  overflowY:'auto',
                                              }}>
                   {/*DESCRIPTION*/}
                   <p style={{
                       margin: '2rem',
                       fontFamily:'monospace',
                       opacity: 0.8,
                       maxWidth: '75vmin',
                       minWidth: '45vmin',
                       fontSize: '2rem',
                       color: GRUVBOX.light2,
                       whiteSpace:'pre'
                   }}>{reader.description}</p>
                 </div>}
                                              {reader.index===2&&<div style={{
                                                  height: 'calc(100% - 5rem)',
                                                  overflowY:'auto',
                                                  display:'flex',
                                                  flexDirection:'column',
                                                  alignItems:'center',
                                              }}>
                                    {/*INGREDIENTS*/}
                                    {reader.requires && Object.entries(reader.requires).map(([id, amount])=>{
                                        return (<div style={{
                                            width: '90%',
                                            display:'grid',
                                            alignItems:'center',
                                            gridTemplateColumns: '50% 25% 25%',
                                            gridTemplateRows: 'repeat(auto-fill, 5rem)'
                                        }}>
      <p>{(()=>{
          let name = groups_object[id];
          if(name) name=name.name;
          if(!name && products_by_group[id] && products_by_group[id][0]) name = products_by_group[id][0].brand+': '+products_by_group[id][0].name;
          if(!name && products_object[id]) name=products_object[id].brand+': '+products_object[id].name
          if(!name || name.length===2) name=lang.RecipeView.reader.unnamedpord||"Unknown Group/Product"
          return name
      })([groups_object, products_by_group, id])}</p>{(products_by_group[id]||products_object[id])&&(()=>{
          let counter = 0;
          if(products_by_group[id]) {
              products_by_group[id].forEach(({stock=0})=>counter+=stock)
          } else if (products_object[id]) {
              counter+=products_object[id].stock||0
          }
          return (
              <p style={{
                  color: (counter>=amount)?GRUVBOX.neutral_green:GRUVBOX.neutral_red
              }}>{counter}{"/"}{amount}</p>
          )
      })([id, groups_object, products_by_group])}<p>( {amount} )</p>
    </div>);
                                    })}
                                  </div>}
                                              {/*Reader Navbar*/}
                                              <div style={{
                                                  background: GRUVBOX.dark0_hard,
                                                  width:'100%',
                                                  height:'5rem',
                                                  bottom: 0,
                                                  left: 0,
                                                  position:'fixed',
                                                  display:'flex',
                                                  justifyContent:'space-evenly',
                                                  alignItems:'center'
                                              }}>                       {reader.index>0&&<IconButton dimens={['2rem', '2rem']} color={GRUVBOX.light2} hover={GRUVBOX.dark3} onClick={()=>{
                                                  // previous page
                                                  setReader(x=>{x.index-=1; return x});
                                              }} icon={<Feather.ChevronLeft/>} title={lang.RecipeView.reader.prev||"Previous"}/>}
                                                <IconButton title={lang.RecipeView.reader.exit||"Close"} dimens={['2rem', '2rem']} color={GRUVBOX.light2} hover={GRUVBOX.dark3} onClick={()=>{
                                                    setReader(()=>null);
                                                }} icon={<Feather.X/>}/>
                                                {reader.index<2&&<IconButton title={lang.RecipeView.reader.next||"Next"} dimens={['2rem', '2rem']} color={GRUVBOX.light2} hover={GRUVBOX.dark3} onClick={()=>{
                                                    setReader(x=>{x.index+=1; return x});
                                                }} icon={<Feather.ChevronRight/>}/>}
                                              </div>
                                            </div>}

          {/*EDIT POPUP*/}
          <EditPopup edit={content.id?content.id.data:null} name={lang.RecipeView.unnamed||"A recipie"} onCancel={()=>{
              edit_done();
              setContent(x=>{return {}});
          }} content={content}
                     set={setContent} onSave={async ()=>{
                         set(x=>{x.loading+=1; return x});
                         let payload = Object.fromEntries(Object.entries(content).map(([key, {data}])=>[key, data]));
                         if(payload.id===true) delete payload.id;
                         const { error } = await state.supabase.client
                                                      .from('Recipes')
                                                      .upsert(payload);
                         set(x=>{x.loading-=1; return x});
                         if(error) {
                             alert(lang.RecipeView.edit.errors.save||"An error occured saving the recipie");
                         }
                         edit_done();
                         setContent(x=>{return {}});
                     }}/>
          {/*RECIPIE LIST*/}
          <div style={StyleSheet.list}>
            {/*SEARCHBAR*/}
            <div style={{
                positon:'sticky',
                top:0,
                left: 0,
                width:'calc(100% - 4rem)',
                padding: '0.4rem',
                margin:'0.6rem 4rem',
                borderRadius: 12.5,
                height: '2rem',
                background:GRUVBOX.dark2,
                display:'flex',
                alignItems:'center'
            }}>
              <IconButton title={lang.RecipeView.saf.filter||"Filter"} dimens={['1.5rem', '1.5rem']} onClick={()=>barMode===1?setBarMode(0):setBarMode(1)} icon={<Feather.Filter/>} hover={GRUVBOX.dark4} color={GRUVBOX.light0} style={{padding:'0.4rem', margin:'0.2rem'}}/>
              <IconButton title={lang.RecipeView.saf.search || "Search"} dimens={['1.5rem', '1.5rem']} onClick={()=>barMode===2?setBarMode(0):setBarMode(2)} icon={<Feather.Search/>} hover={GRUVBOX.dark4} color={GRUVBOX.light0} style={{padding:'0.4rem', margin:'0.2rem'}}/>

              <div style={{
                  display:'flex',
                  flex: 1,
                  alignItems:'center',
              }}>
                {barMode===2&&<input style={{
                    flex: 1,
                    width:'100%',
                    background: GRUVBOX.dark1,
                    color: GRUVBOX.light1,
                    border:'none',
                    outline:'none',
                    padding: '0.6rem 0.4rem',
                    borderRadius: 12.5
                }} value={filter} placeholder={lang.RecipeView.saf.search||"SEARCH"} onChange={e=>setFilter(e.target.value)}/>}
                {barMode===1&&<div style={{
                    display:'flex',
                    flex: 1,
                    alignItems:'center',
                    justifyContent:'space-between'
                }}>
              <IconButton title={lang.RecipeView.saf.f.prev||"Previous"} icon={<Feather.ChevronLeft/>} dimens={['1rem','1rem']} color={GRUVBOX.light1} hover={GRUVBOX.dark1} onClick={()=>{
                  if(sortMode===0) {
                      setSortMode(SORT_MODE_NAMES.length-1);
                  } else {
                      setSortMode(sortMode-1);
                  }
              }} style={{padding: '0.4rem'}}/>
              <p>{lang.RecipeView.saf.f.pre||"Sort by: "}{SORT_MODE_NAMES[sortMode]}</p>
              <IconButton title={lang.RecipeView.saf.f.next||"Next"} icon={<Feather.ChevronRight/>} dimens={['1rem','1rem']} color={GRUVBOX.light1} hover={GRUVBOX.dark1} onClick={()=>{
                  if(sortMode===SORT_MODE_NAMES.length-1) {
                      setSortMode(0);
                  } else {
                      setSortMode(sortMode+1);
                  }
              }} style={{padding: '0.4rem'}}/>
            </div>}
              </div>
              <IconButton title={lang.RecipeView.saf.clear||"Clear"} dimens={['1.5rem', '1.5rem']} onClick={()=>{setFilter(""); setSortMode(0)}} icon={<Feather.X/>} hover={GRUVBOX.dark4} color={GRUVBOX.light0} style={{padding:'0.4rem', margin:'0.2rem'}}/>
            </div>
            {recipies.length===0&&<p>{lang.RecipeView.norecipes||"No Recipes"}</p>}
            {recipies.map(({id="", name=(lang.RecipeView.unnamed||"Unknown Recipie"), thumbnail=null, tags=[], description="", requires={}})=>{
                try {
                    requires = JSON.parse(requires)
                } catch(e) {
                    requires={}
                }
                let status = lang.RecipeView.status.hasitall||"You have all the required stuff";
                Object.entries(requires).forEach(([id, amount], i ,a)=>{
                    let counter = 0;

                    if(products_by_group[id]) {
                        products_by_group[id].forEach(({stock=0})=>counter+=stock)
                    } else if(products_object[id]) {
                        counter+=products_object[id].stock||0
                    }

                    if(counter<amount) {
                        status=lang.RecipeView.status.missing||"Missing ingredients"
                    }
                })

                return (
                    <div style={StyleSheet.listItem}>
                      {thumbnail && <img src={thumbnail} alt={name} style={StyleSheet.itemThumbnail}/>}
                      {!thumbnail && <div style={StyleSheet.itemThumbnail}></div>}
                      <div style={StyleSheet.itemText}>
                        <p style={StyleSheet.itemName}>{name}</p>
                        <p style={StyleSheet.itemID}>{status}</p>
                      </div>
                      <div style={StyleSheet.itemButtons}>
                        <IconButton title={lang.RecipeView.opts.view||"Read"} onClick={()=>{
                            // read
                            setReader(()=>{
                                return {
                                    index: 0,
                                    name: name,
                                    id: id,
                                    ...recipies_object[id],
                                    requires: requires,
                                  tags: tags
                                }
                            })
                        }} dimens={['2rem', '2rem']} style={{padding:'0.4rem'}} icon={<Feather.Coffee/>} hover={GRUVBOX.dark4} color={GRUVBOX.light2}/>


                        <IconButton title={lang.RecipeView.opts.edit||"Edit"} onClick={()=>{
                            // Open EDIT Dialog
                            state.recipes.forEach(obj=>{
                                if(obj.id===id) {
                                    let d = {};
                                    Object.entries(obj).forEach(([key, value])=>{
                                        d[key]={
                                            data: value||"",
                                            placeholder: ({})[key]||key,
                                            type: (["description"].includes(key))?'textarea':'input'
                                        }
                                    });
                                    setContent(()=>{
                                        return d;
                                    })
                                }
                            })
                        }} dimens={['2rem', '2rem']} style={{padding:'0.4rem'}} icon={<Feather.Edit2/>} hover={GRUVBOX.dark4} color={GRUVBOX.light2}/>
                        <IconButton title={lang.RecipeView.opts.delete||"Delete"} onClick={async ()=>{
                            // Delete
                            if (window.confirm(format(lang.RecipeView.delete.confirm||'Do you really want to delete {}?',name))) {
                                set(x=>{x.loading=x.loading+1; return x});
                                const {error} = await state.supabase.client
                                                           .from("Recipes")
                                                           .delete()
                                                           .match({id: id});
                                set(x=>{x.loading=x.loading-1; return x});
                                if(error) {
                                    alert(lang.RecipeView.delete.error||"An Error occured")
                                }
                            }
                        }} dimens={['2rem', '2rem']} style={{padding:'0.4rem'}} icon={<Feather.Trash/>} hover={GRUVBOX.dark4} color={GRUVBOX.light2}/>

                      </div>
                    </div>
                );
            })}
          </div>
          {/*ADD BUTTON*/}

          <IconButton onClick={()=>{
              let d = {};
              Object.entries({
                  name:"",
                  description:"",
                  tags:"{}",
                  requires:"{}",
                  thumbnail:null,
                  id: true
              }).forEach(([key, value])=>{
                  d[key]={
                      data: value||"",
                      placeholder: ({})[key]||key,
                      type: (["description", "requires"].includes(key))?'textarea':'input'
                  }
              });
              setContent(()=>{
                  return d;
              })
          }} dimens={['3rem','3rem']} icon={<Feather.Plus/>} background={GRUVBOX.faded_orange} hover={GRUVBOX.faded_yellow} color={GRUVBOX.light0} style={{
              position: 'fixed',
              right: 0,
              bottom: '5rem',
              padding:'0.4rem',
              display:'flex',
              alignItems:'center',
              justifyContent:'center',
              margin: '1rem'
          }}/>
        </div>
    )
}
const StyleSheet = {
    list: {
        display:'flex',
        flexDirection:'column',
        alignItems:'center',
        overflowY:'auto'
    },
    listItem: {
        display:'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        width: 'calc(100% - 1rem - 5rem - 1rem)',
        margin: '0.4rem 0.5rem',
        padding: '0.4rem 1rem',
        alignItems:'center',
        justifyContent:'space-between',
        position: 'relative',
        paddingLeft: '5rem',
        background: GRUVBOX.dark0,
        borderRadius: 8
    },
    itemThumbnail: {
        borderRadius: '50%',
        background: GRUVBOX.faded_aqua,
        width: '2.5rem',
        height: '2.5rem',
        position: 'absolute',
        left: '1rem'
    },
    itemText: {
        display:'flex',
        flexDirection: 'column',
        alignItems:'flex-start',
        justifyContent:'center'
    },
    itemName: {
        fontWeight: 'bold',
        margin: 0,
    },
    itemID: {
        opacity: 0.5,
        fontSize:'0.8rem',
        margin: 0,
    },
    itemButtons: {
        display:'flex',
        flexWrap:'wrap',
        alignItems:'center',
        justifyContent:'center'
    }
}
