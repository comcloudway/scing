import * as React from 'react';
import bwipjs from 'bwip-js';
import * as Feather from 'react-feather';

import * as GRUVBOX from '../lib/gruvbox-colors';
import Button from '../components/Button';
import IconButton from '../components/IconButton';
import Card from '../components/Card';
import {LanguageContext, LANGUAGES} from '../lib/translate';

export default function ToolsView({state={}, set=()=>{}}) {
    const share_code_generator = React.useRef();
    const lang = React.useContext(LanguageContext);
  const [langput, setLangput] = React.useState(lang.ID);

    React.useEffect(()=>{
        let canvas = bwipjs.toCanvas('share_code_canvas', {
            bcid:        'datamatrix',       // Barcode type
            text:        JSON.stringify({
                from: state.user.name,
                SUPABASE_KEY: state.supabase.key,
                SUPABASE_URL: state.supabase.url
            }),    // Text to encode
            scale:       3,               // 3x scaling factor
            // height:      100,              // Bar height, in millimeters
            includetext: true,            // Show human-readable text
            textxalign:  'center',        // Always good to set this
        });
        canvas.style.background=GRUVBOX.light0;
    }, [state.user.name, state.supabase.key, state.supabase.url]);

    return (
        <div style={{
            display:'flex',
            alignItems:'center',
            flexDirection:'column',
            overflowY:'auto'
        }}>

          <Card background={GRUVBOX.dark2} hover={GRUVBOX.dark3} shadow={GRUVBOX.dark4} onClick={()=>{
              if (window.navigator && window.navigator.share) {
                  (async ()=>{ try {
                      set(x=>{x.loading=x.loading+1; return x});
                      let base64url = share_code_generator.toDataUrl();
                      const blob = await (await fetch(base64url)).blob();
                      const file = new File([blob], 'join-my-scing.png', { type: blob.type });

                      navigator.share({
                        title: lang.ToolsView.share.title||'Join my SCING',
                        text: lang.ToolsView.share.text||'Hey! Scan this code with your SCING client and join my SCING. Get the App over at https://scing.ccw.icu',
                          files: [file],
                      })

                      set(x=>{x.loading=x.loading-1; return x});
                  } catch(e) {
                      set(x=>{x.loading=x.loading-1; return x});

                  }

                             })()
              } else {
                alert(lang.ToolsView.share.unsupported||"SHARING ISN'T SUPPOERTED ON YOUR PLATFORM")
              }
          }} children={[
            <h2>{lang.ToolsView.share.cta||"Let a friend scan this code, so he can join your SCING"}</h2>,
              <canvas style={StyleSheet.qr} ref={share_code_generator} id="share_code_canvas"></canvas>
          ]} style={StyleSheet.card}/>
          <Button style={StyleSheet.buttons} text={lang.ToolsView.logout||"Logout"} onClick={()=>{
              localStorage.clear();
              window.location.reload();
          }} background="transparent" hover={GRUVBOX.faded_orange} color={GRUVBOX.light2}/>
          {window.deferredPrompt&&<Button style={StyleSheet.buttons} text={lang.ToolsView.install||"Install"} onClick={()=>{
              window.deferredPrompt.prompt();
    // Wait for the user to respond to the prompt
              window.deferredPrompt.userChoice.then((choiceResult) => {
                  window.deferredPrompt = null;
      });
          }} background="transparent" hover={GRUVBOX.faded_orange} color={GRUVBOX.light2}/>}
          {/*LANGUAGE*/}
          <Card children={[
            <p>{lang.ToolsView.lang.text||"Language: "}</p>,
            <input value={langput} onChange={e=>setLangput(e.target.value)} style={{
              width: '3ch',
              padding: '0.8rem 0.4rem',
              height:'auto',
              background: GRUVBOX.dark1,
              borderLeft:'2rem solid ' + (LANGUAGES[langput])?GRUVBOX.faded_aqua:GRUVBOX.faded_red,
              border: 'none',
              color: GRUVBOX.light3,
              borderRadius: 12.5,
              outline: 'none',
              marginLeft: '0.8rem'
            }}/>,
            ...[LANGUAGES[langput]&&lang.ID!==langput?<IconButton style={StyleSheet.langbtn} title={lang.ToolsView.lang.save||"Save"} dimens={['2rem', '2rem']} icon={<Feather.Check/>} color={GRUVBOX.light0} hover={GRUVBOX.dark0_hard} onClick={()=>{
              localStorage.setItem("LANG", langput);
            }}/>:''],
            ...[langput!==lang.ID?<IconButton style={StyleSheet.langbtn} title={lang.ToolsView.lang.reset||"Reset"} dimens={['2rem', '2rem']} icon={<Feather.X/>} color={GRUVBOX.light0} hover={GRUVBOX.dark0_hard} onClick={()=>{
              setLangput(lang.ID);
            }}/>:'']
          ]} style={{
            display:'flex',
            flexDirection:'row',
            alignItems: 'center'
          }} background={GRUVBOX.dark0} color={GRUVBOX.light0} hover={GRUVBOX.dark0}/>
        </div>
    );
}
const StyleSheet = {
    card: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: '2rem',
        margin: '1rem'
    },
    qr: {
        margin: '1rem',
        padding: '1rem',
        borderRadius: 12
    },
    langbtn: {
      padding: '0.4rem',
      margin: '0.2rem'
    }
}
