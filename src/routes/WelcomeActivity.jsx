/*
 * Activity launched before user signs up; Shows choose dialog
 */
import * as React from 'react';

import * as GRUVBOX from '../lib/gruvbox-colors';
import * as Feather from 'react-feather';

import Card from '../components/Card';
import JoinActivity from './JoinActivity';
import ConnectActivity from './ConnectActivity';
import Button from '../components/Button';
import {LanguageContext, LANGUAGES} from '../lib/translate';

export default function WelcomeActivity({updateRootView=()=>{}}) {
  const lang = React.useContext(LanguageContext);
  return (
    <div style={{
      background: GRUVBOX.dark0_hard,
      height: '100%',
      width: '100%',
      display: 'flex',
      flexDirection: 'row',
      flexWrap: 'wrap',
      alignItems: 'flex-start',
      justifyContent: 'space-evenly'
    }}>
    <div style={{
      width: '100%',
      display:'flex',
      background: GRUVBOX.dark0,
      top: 0,
      left: 0,
      position:'sticky',
      padding: '0.4rem 0'
    }}>{Object.entries(LANGUAGES).map(([id, {ID, LANGUAGE:name}])=>{
      return (<Button style={{
        padding:'0.4rem'
      }} hover={lang.ID===ID?GRUVBOX.neutral_aqua:GRUVBOX.light4} background={'transparent'} color={lang.ID===ID?GRUVBOX.light4:GRUVBOX.dark4} onClick={()=>{
       localStorage.setItem("LANG", id);
     }} text={name}/>)
    })}</div>

      <Card background={GRUVBOX.dark2} hover={GRUVBOX.dark3} shadow={GRUVBOX.dark4} onClick={()=>updateRootView(<JoinActivity updateRootView={updateRootView}/>)} children={[
        <Feather.Maximize size={48}/>,
        <p style={{margin: 0}}>{lang.WelcomeActivity.scan||"Scan"}</p>
      ]} style={StyleSheet.card}/>

      <Card background={GRUVBOX.dark2} hover={GRUVBOX.dark3} shadow={GRUVBOX.dark4} onClick={()=>updateRootView(<ConnectActivity/>)} children={[
        <Feather.Plus size={48}/>,
        <p style={{margin: 0}}>{lang.WelcomeActivity.connect||"Connect"}</p>
      ]} style={StyleSheet.card}/>


    </div>
  );
};

const StyleSheet = {
  card: {
    color: GRUVBOX.light2,
    width: '40vmin',
    height: '40vmin',
    alignItems: 'center',
    justifyContent: 'space-evenly',
    display: 'flex',
    flexDirection: 'column',
    fontSize: '2rem',
    fontFamily: 'monospace'
  }
}
